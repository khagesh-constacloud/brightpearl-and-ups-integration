<!DOCTYPE html>
<html dir="ltr" lang="en">
@section('title')
Sync Configuration
@endsection
@include('include.header_css')
<style>
	.frmSearch {
		border: 1px solid #a8d4b1;
		background-color: #c6f7d0;
		margin: 2px 0px;
		padding: 40px;
		border-radius: 4px;
	}
	#country-list {
		float: left;
		list-style: none;
		margin-top: -3px;
		padding: 0;
		width: 305px;
		position: absolute;
	}
	#country-list li {
		padding: 4px;
		background: #f0f0f0;
		border-bottom: #bbb9b9 1px solid;
		font-size: 12px;
	}
	#country-list li:hover {
		background: #ece3d2;
		cursor: pointer;
	}
	#search-box {
		padding: 4px;
		border: #a8d4b1 1px solid;
		border-radius: 4px;
	}
	.empty_inp {
		border: 1px solid red !important;
	}
	.select2 {
		width: 100% !important;
	}
	.custom-control-label,
	.control-label {
		font-weight: normal
	}
	.toggle.btn {
		min-width: 60px;
		min-height: 30px;
		border-radius: 20px;
		margin-left: 14px;
	}
	.checkbox-inline .toggle {
		margin-left: 10px !important;
		margin-right: 5px;
	}
	.show_hide {
		display: none;
	}
	.select2-selection--multiple {
		overflow: hidden !important;
		height: auto !important;
	}
	.card-title {
		font-size: 16px !important;
	}
</style>
<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pages.scss -->
		<!-- ============================================================== -->
		@include('include.header')
		<!-- ============================================================== -->
		<!-- End Topbar header -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<div class="page-breadcrumb">
				<div class="row">
					<div class="col-5 align-self-center">
						<h4 class="page-title">Sync Configuration</h4>
					</div>
					<div class="col-7 align-self-center">
						<div class="d-flex align-items-center justify-content-end">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item">
										<a href="#">Home</a>
									</li>
									<li class="breadcrumb-item active" aria-current="page">Sync Configuration</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">

				@if(session()->has('success'))
				<div class="alert alert-success alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<i class="fa fa-check-circle"></i> <small>{{ session()->get('success') }}</small>
				</div>
				@endif
				@if(session()->has('error'))
				<div class="alert alert-danger alert-block">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<i class="fa fa-warning"></i> <small>{{ session()->get('error') }}</small>
				</div>
				@endif
				<!-- ============================================================== -->
				<!-- Sales chart -->
				<!-- ============================================================== -->
				<div class="card-group">
					<div class="card">
						<!--<div class="card-body">-->
						<div class="row">
							<div class="col-md-12">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item"> <a class="nav-link active" id="shipment-tab" data-toggle="tab"
											href="#orders" role="tab" aria-controls="orders" aria-expanded="true"><span
												class="hidden-sm-up"><i class="fa fa-shipping-fast"></i></span> <span
												class="hidden-xs-down">Shipment</span></a> </li>
								</ul>
								<div class="tab-content tabcontent-border p-20" id="myTabContent">
									<!-- ============================================================== -->
									<!-- Shipment Setting tab -->
									<!-- ============================================================== -->
									<div role="tabpanel" class="tab-pane fade show active" id="orders"
										aria-labelledby="shipment-tab">
										<p>
										<div class="card-body">
											<div class="row">
												<div class="col-md-12">
													<form class="needs-validation form-horizontal save_order_form"
														novalidate action="{{route('save-shipment-setting')}}"
														method="POST">
														@csrf
														<input type="hidden" id="hidden_organization_id"
															value="{{Auth::User()->organization_id}}" />

														{{-- Form Actions --}}
														@if($bp_account->status===1)
														<div class="card-body">
															<div class="action-form">
																<div class="form-group m-b-0 text-left">
																	<button type="submit"
																		class="btn btn-success bp_order_save_button"> <i
																			class="fa fa-check"></i>
																		Save</button>
																	<button type="button" onclick="window.location='{{ url('/configuration') }}'"
																		class="btn btn-dark">Cancel</button>
																</div>
															</div>
														</div>
														<hr>
														@endif
														<div class="card-body">

															<div class="tab-content tabcontent-border">
																{{-- Shipments --}}
																<div class="tab-pane p-20 active" id="shipments" role="tabpanel">

																	{{-- Enable shipment sync --}}
																	{{-- <div class="card-body">
																		<div class="row">
																			<div class="col-md-5">
																				<div class="custom-control custom-checkbox"
																					style="margin-bottom: 20px;">
																					<input type="checkbox"
																						class="custom-control-input allow_product_sync"
																						name="allow_product_sync" id="allow_product_sync"
																						{{$product->allow_product_sync=="1"?'checked':''}}
																					value="1"
																					data-fulfillment="{{$product->allow_product_sync}}"
																					>
																					<label class="custom-control-label"
																						for="allow_product_sync" style="font-size: 15px;">
																						Enable Product Sync <span
																							class="badge badge-pill badge-info ml-auto m-r-15"
																							data-toggle="tooltip" data-placement="top"
																							title="Enable or Disable Product sync through this switch">?</span>
																					</label>
																					<small id="error_allow_product_sync"></small>
																				</div>
																			</div>
																		</div>
																	</div>
																	<hr> --}}

																	{{-- Default Shipping Methods --}}
																	<div class="card-body">
																		<h4 class="card-title">Set default Shipping Method for GON Tracking information update in
																			Brightpearl
																			<span
																				class="btn waves-effect refresh_bp_shipping_method waves-light btn-sm btn-rounded btn-info"
																				data-toggle="tooltip" data-placement="top"
																				title="Refresh Brightpearl Shipping Method."><i class="mdi mdi-reload "></i>
																			</span>
																		</h4>
																		<div class="form-group row">
																			<label for="matchedby" class="col-sm-3 control-label col-form-label">Choose
																				Default Shipping Method
																				<span class="badge badge-pill badge-info ml-auto m-r-15"
																					data-toggle="tooltip" data-placement="top"
																					title="Select a default Shipping method to get sent to Brightpearl while updating Goods Out Note.">?</span>
																			</label>
																			<div class="col-sm-3">
																				<label for="bp_default_shipping_method"
																					class=" control-label col-form-label">Brightpearl Shipping Method
																					<span class="text-danger">*</span>
																				</label>
																				<div class="form-group">
																					<select class="form-control select2 bp_default_shipping_method"
																						name="bp_default_shipping_method" id="bp_default_shipping_method" style="width:100%"
																						required>
																					</select>
																					<small class="error_msg" id="error_bp_default_shipping_method">Field cannot
																						be empty</small>
																				</div>
																			</div>
																		</div>
																	</div>
																	<hr>

																	{{-- Allow Brightpearl Warehouse Filter for Shipment --}}
																	<div class="card-body">
																		<div class="custom-control custom-checkbox" style="margin-bottom: 20px;">
																			<input type='checkbox' class='custom-control-input allow_warehouse_filter'
																				name='allow_warehouse_filter' id='allow_warehouse_filter' value='1'
																				{{ isset($shipment->allow_warehouse_filter) && $shipment->allow_warehouse_filter ? 'checked' : '' }}>
																			<label class="custom-control-label" for="allow_warehouse_filter"
																				style="font-size: 15px;">
																				Allow Warehouse Filter
																			</label>
																			<br>
																			<span>Enable to have BrightPearl Warehouse Filter.</span>
																			<span
																				class="btn waves-effect refresh_bp_warehouse waves-light btn-sm btn-rounded btn-info"
																				data-toggle="tooltip" data-placement="top"
																				title="Refresh Brightpearl Shipping Method."><i class="mdi mdi-reload "></i>
																			</span>
																		</div>
																		<div class="form-group row sec-warehouse-select {{ isset($shipment->allow_warehouse_filter) && $shipment->allow_warehouse_filter ? '' : 'hide' }}">
																			<label for="matchedby" class="col-sm-3 control-label col-form-label">Select
																				Warehouse(s)
																				<span class="badge badge-pill badge-info ml-auto m-r-15"
																					style="margin-right: 0;" data-toggle="tooltip" data-placement="top"
																					title="Warehouse Filter on Shipment sync from Brightpearl">?</span>
																			</label>
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label for="bp_default_warehouse"
																						class="control-label col-form-label">Brightpearl Warehouse<span
																							class="text-danger">*</span>
																					</label>
																					<select class="form-control bp_default_warehouse select2"
																						multiple="multiple" data-placeholder="Select Warehouse(s)" data-dropdown-css-class="select2-purple"
																						name="bp_default_warehouse[]" style="width:100%" id="bp_default_warehouse"
																						required>
																					</select>
																					<small class="error_msg" id="error_bp_default_warehouse">Field cannot
																						be empty</small>
																				</div>
																			</div>
																		</div>
																	</div>
																	<hr>

																	{{-- Allow Brightpearl Order Status Filter for Shipment --}}
																	<div class="card-body">
																		<div class="custom-control custom-checkbox" style="margin-bottom: 20px;">
																			<input type='checkbox' class='custom-control-input allow_status_filter'
																				name='allow_status_filter' id='allow_status_filter' value='1'
																				{{ isset($shipment->allow_status_filter) && $shipment->allow_status_filter ? 'checked' : '' }}>
																			<label class="custom-control-label" for="allow_status_filter"
																				style="font-size: 15px;">
																				Allow Order Status Filter
																			</label>
																			<br>
																			<span>Enable to have BrightPearl Order Status Filter.</span>
																			<span
																				class="btn waves-effect refresh_bp_order_status waves-light btn-sm btn-rounded btn-info"
																				data-toggle="tooltip" data-placement="top"
																				title="Refresh Brightpearl Shipping Method."><i class="mdi mdi-reload "></i>
																			</span>
																		</div>
																		<div class="form-group row sec-status-select {{ isset($shipment->allow_status_filter) && $shipment->allow_status_filter ? '' : 'hide' }}">
																			<label for="matchedby" class="col-sm-3 control-label col-form-label">Select
																				Order Status
																				<span class="badge badge-pill badge-info ml-auto m-r-15"
																					style="margin-right: 0;" data-toggle="tooltip" data-placement="top"
																					title="Channel Filter on Shipment sync from Brightpearl">?</span>
																			</label>
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label for="bp_default_order_status"
																						class="control-label col-form-label">Brightpearl Order Status<span
																							class="text-danger">*</span>
																					</label>
																					<select class="form-control bp_default_order_status select2"
																						multiple="multiple" data-placeholder="Select Order Status" data-dropdown-css-class="select2-purple"
																						name="bp_default_order_status[]" style="width:100%" id="bp_default_order_status"
																						required>
																					</select>
																					<small class="error_msg" id="error_bp_default_order_status">Field cannot
																						be empty</small>
																				</div>
																			</div>
																		</div>
																	</div>
																	<hr>

																	{{-- Allow Brightpearl Shipping Method Filter for Shipment --}}
																	<div class="card-body hide">
																		<div class="custom-control custom-checkbox" style="margin-bottom: 20px;">
																			<input type='checkbox' class='custom-control-input allow_ship_method_filter'
																				name='allow_ship_method_filter' id='allow_ship_method_filter' value='1'
																				{{ isset($shipment->allow_ship_method_filter) && $shipment->allow_ship_method_filter ? 'checked' : '' }}>
																			<label class="custom-control-label" for="allow_ship_method_filter"
																				style="font-size: 15px;">
																				Allow Shipping Method Filter
																			</label>
																			<br>
																			<span>Enable to have BrightPearl Shipping Method Filter.
																			</span>
																		</div>
																		<div class="form-group row sec-ship-method-select {{ isset($shipment->allow_ship_method_filter) && $shipment->allow_ship_method_filter ? '' : 'hide' }}">
																			<label for="matchedby" class="col-sm-3 control-label col-form-label">Select
																				Shipping Method(s)
																				<span class="badge badge-pill badge-info ml-auto m-r-15"
																					style="margin-right: 0;" data-toggle="tooltip" data-placement="top"
																					title="Shipping Method Filter on Shipment sync from Brightpearl">?</span>
																			</label>
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label for="bp_default_shipping_method"
																						class="control-label col-form-label">Brightpearl Shipping Method<span
																							class="text-danger">*</span>
																					</label>
																					<select class="form-control bp_default_shipping_method select2"
																						multiple="multiple" data-placeholder="Select channel(s)" data-dropdown-css-class="select2-purple"
																						name="bp_default_shipping_method" style="width:100%" id="bp_default_shipping_method"
																						required>
																					</select>
																					<small class="error_msg" id="error_bp_default_shipping_method">Field cannot
																						be empty</small>
																				</div>
																			</div>
																		</div>
																	</div>


																	{{-- Allow Brightpearl GON Status Filter for Shipment --}}
																	<div class="card-body hide">
																		<div class="custom-control custom-checkbox" style="margin-bottom: 20px;">
																			<input type='checkbox' class='custom-control-input allow_gon_status_filter'
																				name='allow_gon_status_filter' id='allow_gon_status_filter' value='1'
																				{{ isset($shipment->allow_gon_status_filter) && $shipment->allow_gon_status_filter ? 'checked' : '' }}>
																			<label class="custom-control-label" for="allow_gon_status_filter"
																				style="font-size: 15px;">
																				Allow GON Status Filter
																			</label>
																			<br>
																			<span>Enable to have BrightPearl GON Status Filter.
																			</span>
																		</div>
																		<div class="form-group row sec-gon-status-select {{ isset($shipment->allow_gon_status_filter) && $shipment->allow_gon_status_filter ? '' : 'hide' }}">
																			<label for="matchedby" class="col-sm-3 control-label col-form-label">Select
																				GON Status(s)
																				<span class="badge badge-pill badge-info ml-auto m-r-15"
																					style="margin-right: 0;" data-toggle="tooltip" data-placement="top"
																					title="GON Status Filter on Shipment sync from Brightpearl">?</span>
																			</label>
																			<div class="col-sm-6">
																				<div class="form-group">
																					<label for="bp_default_gon_status"
																						class="control-label col-form-label">Brightpearl GON Status<span
																							class="text-danger">*</span>
																					</label>
																					<select class="form-control bp_default_gon_status select2"
																						multiple="multiple" data-placeholder="Select GON Status" data-dropdown-css-class="select2-purple"
																						name="bp_default_gon_status" style="width:100%" id="bp_default_gon_status"
																						required>
																					</select>
																					<small class="error_msg" id="error_bp_default_gon_status">Field cannot
																						be empty</small>
																				</div>
																			</div>
																		</div>
																	</div>

																	{{-- Option to allow item price sync from which source --}}
																	<div class="card-body">
																		<h4 class="card-title">Select the default price-list for invoice line unit price</h4>
																		<div class="row">
																			<!-- Sync flow section -->
																			<div class="col-md-12">
																				<div class="form-group row hide">
																					<label for="fname"
																						class="col-sm-3  control-label col-form-label">Select the option
																						<span class="text-danger">*</span>
																					</label>
																					<div class="col-sm-9">
																						<div class="form-group">
																							<div class="form-check form-check-inline">
																								<div class="custom-control custom-radio">
																									<input type="radio"
																										{{ isset($shipment->default_item_price_source) && $shipment->default_item_price_source == 'bp_price_list' ? 'checked' : '' }}
																										class="custom-control-input" id="bp_price_list"
																										name="default_item_price_source" value="bp_price_list">
																									<label class="custom-control-label" for="bp_price_list"
																										id="customControlValidation10">Brightpearl Price List</label>
																								</div>
																							</div>
																							<div class="form-check form-check-inline">
																								<div class="custom-control custom-radio">
																									<input type="radio"
																										{{ isset($shipment->default_item_price_source) && $shipment->default_item_price_source == 'order_line' ? 'checked' : '' }}
																										class="custom-control-input" id="order_line"
																										name="default_item_price_source" value="order_line">
																									<label class="custom-control-label" for="order_line"
																										id="customControlValidation11">Order Line</label>
																								</div>
																							</div>
																						</div>
																					</div>
																				</div>

																				<!-- Price list -->
																				<div class="form-group row sec-bp-price-list">
																					<div class="col-sm-3"></div>
																					<div class="col-sm-3">
																						<div class="form-group">
																							<label for="ls_default_register" class="ontrol-label col-form-label">Select a
																								Price List
																								<span class="badge badge-pill badge-info ml-auto m-r-15"
																									style="margin-right: 0;" data-toggle="tooltip" data-placement="top"
																									title="Required for Brightpearl product price sync to set in Commercial Invoice Line price value.">?</span>

																								<span
																									class="btn waves-effect refresh_bp_price_list waves-light btn-sm btn-rounded btn-info"
																									data-toggle="tooltip" data-placement="top"
																									title="Refresh Brightpearl Price List."><i class="mdi mdi-reload "></i>
																								</span>
																							</label>
																							<select class="form-control select2 bp_default_price_list_id"
																								name="bp_default_price_list_id"
																								id="bp_default_price_list_id" style="width:100%">
																							</select>
																							<small class="error_msg" id="error_bp_default_price_list_id">Field cannot
																								be empty</small>
																						</div>
																					</div>
																				</div>

																			</div>

																			<div class="col-md-12">
																				<label class="control-label col-form-label">
																					Note: Specify a price-list for line item price. This setting allows to sync item's unit price for commercial invoice
																					line from the source which will be selected.
																				</label>
																			</div>

																		</div>

																	</div>

																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
										</p>
									</div>
									<!-- ============================================================== -->
									<!-- End Product Setting tab -->
									<!-- ============================================================== -->
								</div>
							</div>
							<!--</div>-->
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- Sales chart -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- footer -->
			<!-- ============================================================== -->
			@include('include.footer')
			<!-- ============================================================== -->
			<!-- End footer -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	@include('include.footer_js')
</body>

<script src="{{ asset('public/js/bootstrap-toggle.min.js') }}"></script>
<script src="{{ asset('public/assets/libs/select2/dist/js/select2.min.js') }}"></script>
<script>
	// $('.allow_product_sync').change(function() {
	// 	if ($(this).prop('checked')) {
	// 		$('.allow_product_sync').attr('value', 1);
	// 		$('.dv_order_conf').css('display','block');
	// 	} else {
	// 		$('.allow_product_sync').attr('value', 0);
	// 		$('.dv_order_conf').css('display','none');
	// 	}
	// });

	function scrolltoTop(element){
		var targetOffset = element.offset().top - $(".topbar").outerHeight(true);
			$('html, body').animate({
			scrollTop: targetOffset
			}, 'slow');
	}

	$(".bp_order_save_button").click(function(e) {
		var vald =  validateMyForm_order();
		if(vald==false){
			e.preventDefault();
		}
	});

	function validateMyForm_order() {
		var flag = false;
		if ($('#start_order_id_for_sync_order').val() == "") {
			flag = true;
			//toastr.error('Please Add Start Order ID','',options);
			toastr.error('Please Add Start Order ID');
			$('#error_start_order_id_for_sync_order').empty().text('Please fill the value').css('color', 'red');
		}else if ($('#default_brightpearl_channel').val() == "") {
			flag = true;
			toastr.error('Please select brightpearl channel name');
			$('#error_channel').empty().text('Please fill the value').css('color', 'red');
		} else if($('#default_brightpearl_tax_id').val()==""){
			flag = true;
			toastr.error('Please select brightpearl default Tax');
			$('#error_default_brightpearl_tax_id').empty().text('Please fill the value').css('color', 'red');
		} else if ($('#brightpearl_warehouse_id').val() == "") {
			flag = true;
			toastr.error('Please select brightpearl default warehouse name');
			$('#error_warehouse').empty().text('Please fill the value').css('color', 'red');
		} else if ($('#default_brightpearl_order_status').val() == "") {
			flag = true;
			toastr.error('Please select brightpearl order status');
			$('#error_default_brightpearl_order_status').empty().text('Please fill the value').css('color', 'red');
		} else if ($('#default_bp_cancelled_order_status').val() == "") {
			flag = true;
			toastr.error('Please select brightpearl default cancelled order status');
			$('#error_cancelled_order').empty().text('Please fill the value').css('color', 'red');
		} else if ($('#default_bp_order_edit_status').val() == "") {
			flag = true;
			toastr.error('Please select brightpearl default order edit status');
			$('#error_order_edit_status').empty().text('Please fill the value').css('color', 'red');
		}/* else if ($('#default_bp_product_brand_id').val() == "") {
			flag = true;
			toastr.error('Please select brightpearl default product brand','',options);
			$('#error_product_brand').empty().text('Please fill the value').css('color', 'red');
		}*/
		if (!flag) {
			return true;
		} else {
			return false;
		}
	}

	/*$(".restart_product_sync").click(function(e) {
		if (confirm('Are you sure you want to restart product sync?')) {
			$.ajax({
				type: 'POST',
				url: "{{url('/restartProductSync')}}",
				dataType:'json',
				data: {
					"_token": "{{ csrf_token() }}"
				},
				beforeSend: function() {
					showOverlay();
				},
				success: function(response) {
				hideOverlay();
					if (response.status_code === 1) {
						toastr.success(response.status_text);
					} else {
						toastr.error(response.status_text);
					}
				},
				error: function (jqXHR, textStatus, errorThrown) {
					hideOverlay();
					if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
					} else {
					toastr.error('Unexpected error Please try again.');
					}
				}
			});
		}
	});*/

	$(document).on('click', '.allow_warehouse_filter', function() {
		if( $('[name="allow_warehouse_filter"]:checked').val() ){
			$('.sec-warehouse-select').removeClass('hide');
		}else{
			$('.sec-warehouse-select').addClass('hide');
		}
	});

	$(document).on('click', '.allow_status_filter', function() {
		if( $('[name="allow_status_filter"]:checked').val() ){
			$('.sec-status-select').removeClass('hide');
		}else{
			$('.sec-status-select').addClass('hide');
		}
	});

	$(document).on('click', '.save_button', function() {
		tab = $('.tab-pane.active').attr('id');
		data_post = {
			"_token": "{{ csrf_token() }}",
			'mappings': []
		};
		$('.error_msg').hide();
		if (tab == 'orders') {
			data_post.setting_type = 'order';
			data_post.allow_channel_filter = $('[name="allow_channel_filter"]:checked').val() ? '1' : '0';
			//data_post.allow_order_sync = $('[name="allow_order_sync"]:checked').val() ? '1' : '0';
			data_post.bp_default_sales_channel = $('[name="bp_default_sales_channel"]').val() ? $(
				'[name="bp_default_sales_channel"]').val() : '0';
			data_post.bp_default_shipping_method = $('[name="bp_default_shipping_method"]:checked').val() ? '1' : '0';

			valid = true;
			if(data_post.allow_channel_filter && data_post.allow_channel_filter != 0){
				if( !data_post.bp_default_sales_channel || data_post.bp_default_sales_channel == 0){
					$('#error_bp_default_sales_channel').show();
					$('[name="bp_default_sales_channel"]').focus();
					valid = false;
				}
			}

			if(data_post.allow_channel_filter && data_post.allow_channel_filter != 0){
				if( !data_post.bp_default_sales_channel || data_post.bp_default_sales_channel == 0){
					$('#error_bp_default_sales_channel').show();
					$('[name="bp_default_sales_channel"]').focus();
					valid = false;
				}
			}

			if(data_post.bp_default_shipping_method && data_post.bp_default_shipping_method != 0){
				if( !data_post.bp_default_price_list_id || data_post.bp_default_price_list_id == 0){
					$('#error_bp_default_price_list_id').show();
					$('[name="bp_default_price_list_id"]').focus();
					valid = false;
				}
			}

			if (!valid)
				return false;
		}
		$.ajax({
			type: 'POST',
			url: "{{ url('/AddEditSyncSettings') }}",
			data: data_post,
			beforeSend: function() {
				showOverlay();
			},
			success: function(response) {
				hideOverlay();
				if (response.status_code === 1) {
					toastr.success(response.status_text);
				} else {
					toastr.error(response.status_text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				hideOverlay();
				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
	})

	/*$(function() {
		getBpShipmentInfo();
	});*/
	$( document ).ready(function() {
		getBpShipmentInfo();
	});

	function getBpShipmentInfo() {
		$.ajax({
			type: 'POST',
			url: "{{ url('/getBpShipmentInfo') }}",
			data: {
				"_token": "{{ csrf_token() }}"
			},
			beforeSend: function() {},
			success: function(response) {
				//hideOverlay();
				if (response.status_code === 1) {
					$('.bp_default_warehouse').html('<option value="">--Select--</option>' + response
						.warehouse_opt);
					$('.bp_default_order_status').html('<option value="">--Select--</option>' + response
						.order_status_opt);
					$('.bp_default_shipping_method').html('<option value="">--Select--</option>' + response
						.shipping_method_opt);
					$('.bp_default_price_list_id').html('<option value="">--Select--</option>' + response
						.price_list_opt);

				} else {
					toastr.error(response.status_text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//hideOverlay();
				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
	}

	$(document).on('click', '.refresh_bp_shipping_method', function() { // Refresh default sales BP
		$.ajax({
			type: 'POST',
			url: "{{ url('/refreshBpShippingMethods') }}",
			data: {
				"_token": "{{ csrf_token() }}"
			},
			beforeSend: function() {
				//showOverlay();
			},
			success: function(response) {
				//hideOverlay();
				if (response.status_code === 1) {
					$('.bp_default_shipping_method').html('<option value="">--Select--</option>' +
						response.shipping_method_opt);

					toastr.success(response.status_text);
				} else {
					toastr.error(response.status_text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//hideOverlay();
				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
	});

	$(document).on('click', '.refresh_bp_warehouse', function() { // Refresh default sales BP
		$.ajax({
			type: 'POST',
			url: "{{ url('/refreshBpWarehouses') }}",
			data: {
				"_token": "{{ csrf_token() }}"
			},
			beforeSend: function() {
				//showOverlay();
			},
			success: function(response) {
				//hideOverlay();
				if (response.status_code === 1) {
					$('.bp_default_warehouse').html('<option value="">--Select--</option>' + response
						.warehouse_opt);

					toastr.success(response.status_text);
				} else {
					toastr.error(response.status_text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//hideOverlay();
				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
	});

	$(document).on('click', '.refresh_bp_order_status', function() { // Refresh default sales BP
		$.ajax({
			type: 'POST',
			url: "{{ url('/refreshBpOrderStatus') }}",
			data: {
				"_token": "{{ csrf_token() }}"
			},
			beforeSend: function() {
				//showOverlay();
			},
			success: function(response) {
				//hideOverlay();
				if (response.status_code === 1) {
					$('.bp_default_order_status').html('<option value="">--Select--</option>' + response
						.order_status_opt);

					toastr.success(response.status_text);
				} else {
					toastr.error(response.status_text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//hideOverlay();
				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
	});

	$(document).on('click', '.refresh_bp_price_list', function() { // Refresh default sales BP
		$.ajax({
			type: 'POST',
			url: "{{ url('/refreshBpPriceList') }}",
			data: {
				"_token": "{{ csrf_token() }}"
			},
			beforeSend: function() {
				//showOverlay();
			},
			success: function(response) {
				//hideOverlay();
				if (response.status_code === 1) {
					$('.bp_default_price_list').html('<option value="">--Select--</option>' +
						response.price_list_opt);

					toastr.success(response.status_text);
				} else {
					toastr.error(response.status_text);
				}
			},
			error: function(jqXHR, textStatus, errorThrown) {
				//hideOverlay();
				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
	});

	// function to enable section required for order sync BP to LS
	$('[name="default_item_price_source"]').click(function() {
		var source = $('[name="default_item_price_source"]:checked').val();
		source = source.trim();
		if( source == 'order_line' ){
			$('.sec-bp-price-list').addClass('hide');
		}else{
			$('.sec-bp-price-list').removeClass('hide');
		}
	});

/***************************************************** ENd ORder Settings ****************************/
</script>
</html>