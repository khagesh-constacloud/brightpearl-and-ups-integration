<!DOCTYPE html>
<html dir="ltr" lang="en">
@section('title')
Settings
@endsection
@include('include.header_css')
<body>
	<!-- ============================================================== -->
	<!-- Preloader - style you can find in spinners.css -->
	<!-- ============================================================== -->
	<div class="preloader">
		<div class="lds-ripple">
			<div class="lds-pos"></div>
			<div class="lds-pos"></div>
		</div>
	</div>
	<!-- ============================================================== -->
	<!-- Main wrapper - style you can find in pages.scss -->
	<!-- ============================================================== -->
	<div id="main-wrapper">
		<!-- ============================================================== -->
		<!-- Topbar header - style you can find in pages.scss -->
		<!-- ============================================================== -->
		@include('include.header')
		<!-- ============================================================== -->
		<!-- End Topbar header -->
		<!-- ============================================================== -->
		<!-- ============================================================== -->
		<!-- Page wrapper  -->
		<!-- ============================================================== -->
		<div class="page-wrapper">
			<!-- ============================================================== -->
			<!-- Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<div class="page-breadcrumb">
				<div class="row">
					<div class="col-5 align-self-center">
						<h4 class="page-title">Settings</h4>
					</div>
					<div class="col-7 align-self-center">
						<div class="d-flex align-items-center justify-content-end">
							<nav aria-label="breadcrumb">
								<ol class="breadcrumb">
									<li class="breadcrumb-item">
										<a href="#">Home</a>
									</li>
									<li class="breadcrumb-item active" aria-current="page">Settings</li>
								</ol>
							</nav>
						</div>
					</div>
				</div>
			</div>
			<!-- ============================================================== -->
			<!-- End Bread crumb and right sidebar toggle -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- Container fluid  -->
			<!-- ============================================================== -->
			<div class="container-fluid">
				<!-- ============================================================== -->
				<!-- Sales chart -->
				<!-- ============================================================== -->
				<div class="card-group">
					<div class="card">
						<!--<div class="card-body">-->
						<div class="row">
							<div class="col-md-12">
								<ul class="nav nav-tabs" id="myTab" role="tablist">
									<li class="nav-item"> <a class="nav-link active" id="home-tab" data-toggle="tab"
											href="#home5" role="tab" aria-controls="home5" aria-expanded="true"><span
												class="hidden-sm-up"><i class="mdi mdi-lock"></i></span> <span
												class="hidden-xs-down">API Settings</span></a> </li>
									{{-- <li class="nav-item"> <a class="nav-link" id="profile-tab" data-toggle="tab"
											href="#profile5" role="tab" aria-controls="profile"><span
												class="hidden-sm-up"><i class="mdi mdi-email"></i></span> <span
												class="hidden-xs-down">Email Notifications</span></a></li> --}}
								</ul>
								<div class="tab-content tabcontent-border p-20" id="myTabContent">
									<!-- ============================================================== -->
									<!-- API Setting tab -->
									<!-- ============================================================== -->
									<div role="tabpanel" class="tab-pane fade show active" id="home5"
										aria-labelledby="home-tab">
										<p>
											<div class="card-body">
												@php
													$bp_account_name = "";
													$is_connected = 0;
													if($bp_account_details){
														$bp_account_name = $bp_account_details->account_name;
														$is_connected = 1;
													}
												@endphp
												<div class="card-title">
													<img src="{{asset('public/images/bp.png')}}" style="width:103px;"
														class="float-right mt-10 img-fluid">
												</div>
												<h4 class="card-title">Brightpearl Account - <span
														class="badge badge-pill badge-info ml-auto m-r-15"
														data-toggle="tooltip" data-placement="top" title=""
														data-original-title="Please fill the below field details for Brightpearl account connection.">?</span>
													@if($is_connected==1)
													<span class="badge badge-pill badge-success"><i class="fa fa-check"></i>
														Connected</span>
													@else
													<span class="badge badge-pill badge-danger"><i class="fa fa-times"></i>
														Not Connected</span>
													@endif
												</h4>
												<br>

												<div class="row">

													<div class="col-md-6">
														<div class="form-group">
															<label class="control-label">Brightpearl Account Name <span
																	class="text-danger">*</span></label>
															<input type="text" class="form-control bp_text"
																name="bp_account_name" id="bp_account_name"
																value="<?php echo $bp_account_name;?>" required
																@if($is_connected==1) disabled @endif>
														</div>
													</div>

													<div class="col-md-12">
														<div class="form-group">
															@if($is_connected==1)
																<button type="submit" class="btn btn-danger" id="bp_disconnect_btn"> <i class="fa fa-times"></i> Disconnect</button>
															@else
																<button type="button" class="btn btn-success" id="bp_save_btn" onclick="OpenBpAuthWindow()"> <i class="fa fa-check"></i> Connect</button>
															@endif
														</div>
													</div>

												</div>

												<hr>
											</div>
										</p>
									</div>
									<!-- ============================================================== -->
									<!-- End API Setting tab -->
									<!-- ============================================================== -->
								</div>
							</div>
							<!--</div>-->
						</div>
					</div>
				</div>
				<!-- ============================================================== -->
				<!-- Sales chart -->
				<!-- ============================================================== -->
			</div>
			<!-- ============================================================== -->
			<!-- End Container fluid  -->
			<!-- ============================================================== -->
			<!-- ============================================================== -->
			<!-- footer -->
			<!-- ============================================================== -->
			@include('include.footer')
			<!-- ============================================================== -->
			<!-- End footer -->
			<!-- ============================================================== -->
		</div>
		<!-- ============================================================== -->
		<!-- End Page wrapper  -->
		<!-- ============================================================== -->
	</div>
	<!-- ============================================================== -->
	<!-- End Wrapper -->
	<!-- ============================================================== -->
	<!-- ============================================================== -->
	@include('include.footer_js')
</body>
<script>
	var pop;
	CreateInterval();

	function OpenBpAuthWindow() {

		var account_name = $('#bp_account_name').val();
		if(account_name != ''){
			state = account_name+':'+'{{auth()->user()->org_id}}';

			auth_url = 'https://oauth.brightpearl.com/authorize/'+account_name+'?response_type=code&client_id={{env("BP_CLIENT_ID")}}&redirect_uri={{url("store-oauth-tokens")}}&state='+state

			pop = null;
			clearInterval(checkConnect);
			CreateInterval();
			pop = window.open(auth_url, 'popup', 'width=600,height=600,scrollbars=no,resizable=no');
			// pop = window.location.href= auth_url;
		}else{
			swal('Failed!', 'Brightpearl account name is required.', 'error');
		}
	}

	function CreateInterval() {

		checkConnect = setInterval(function() {

		if (!pop || !pop.closed) return;

		clearInterval(checkConnect);

		$.ajax({
			type: 'POST',
			url: "{{url('getConnectedAccountInfo')}}",
			data: {
				'_token' : "{{ csrf_token() }}", organization_id: "{{auth()->user()->organization_id}}"
			},
			beforeSend: function () {

			},
			success: function (response) {

				console.log(response);
				if (response.ac_connected === 1) {
					toastr.success(response.status_text);
					setTimeout(function(){ location.reload(); }, 4000);
				} else {
					toastr.error(response.status_text);
				}
			},

			error: function (jqXHR, textStatus, errorThrown) {

				if (jqXHR.status == 500) {
					toastr.error('Internal error: ' + jqXHR.responseText);
				} else {
					toastr.error('Unexpected error Please try again.');
				}
			}
		});
		}, 100);
	}

	$("#bp_disconnect_btn").click(function (ep) {
			swal({
				title: "Are you sure?",
				text: "You want to disconnect this account!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Yes, Disconnect it!",
				cancelButtonText: "Cancel",
				closeOnConfirm: false,
				closeOnCancel: false
			})
			.then((willDelete) => {
				if (willDelete.value)
				{
					$('.loaderAjax').css('display', 'block');
					$.get("{{ URL('disconnectBrightpearlAccount') }}").then(function(response){
						swal('Success!', 'Account Disconnected Successfully.', 'success');
						$('.loaderAjax').css('display', 'none');
						location.reload();
					});
				}
			});
	});
</script>
</html>