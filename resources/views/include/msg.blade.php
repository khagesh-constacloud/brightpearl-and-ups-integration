@php
	$params_bp = ['api.organization_id'=>$org_id, 'api.status'=>1];
	$bp_account_connect = DB::table('api_config AS api')->where($params_bp)
	->select('api.organization_id', 'api.account_name', 'api.app_id', 'api.app_secret', 'api.sync_completed')->first();

	$arrcompletemsg = array();

	if( $params_bp && $params_bp->sync_completed != 1 ){
		$arrcompletemsg[] = "<i class='mdi mdi-alert'></i> Please wait before turning ON the Synchronization, Brightpearl Products are getting fetched!";
	}

	@endphp

	@if(count($arrcompletemsg) >  0 && $bp_account_connect)
	<div class="alert alert-warning alert-block">
		<button type="button" class="close" data-dismiss="alert">×</button>
		<small><?php echo implode('<br/>',$arrcompletemsg); ?></small>
	</div>
@endif
