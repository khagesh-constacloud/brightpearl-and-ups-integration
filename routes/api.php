<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::any('webhook_brightpearl_shipment/{org_id}', ['as' => 'webhook_brightpearl_shipment', 'uses' => 'BrightpearlController@receiveShipmentWebhook']);
Route::any('webhook_brightpearl_shipment_destroyed/{org_id}', ['as' => 'webhook_brightpearl_shipment_destroyed', 'uses' => 'BrightpearlController@receiveShipmentDestroyedWebhook']);
Route::any('webhook_brightpearl_so_status_modified/{org_id}', ['as' => 'webhook_brightpearl_so_status_modified', 'uses' => 'BrightpearlController@recieveSoStatusWebhook']);
