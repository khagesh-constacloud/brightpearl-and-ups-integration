<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



/******************* On Use *********************/
Route::get('/', function () {
    return view('auth/login');
});

Route::get('/clear', function() {

    //Artisan::call('cache:clear');
    Artisan::call('config:clear');
    //Artisan::call('config:cache');
    //Artisan::call('view:clear');
    return "Cleared!";

 });
Auth::routes();
Route::get('/test', 'TestController@test');
Route::any('/home', 'HomeController@index')->name('home');
Route::any('/dashboard', 'HomeController@index')->name('dashboard');

Route::any('/settings', 'SettingsController@index')->name('settings');
Route::any('/store-oauth-tokens', 'SettingsController@bpOauthRedirectHandler')->name('store-oauth-tokens');
Route::any('/disconnectBrightpearlAccount', 'SettingsController@disconnectBrightpearlAccount')->name('disconnectBrightpearlAccount');
Route::post('getConnectedAccountInfo', 'SettingsController@getConnectedAccountInfo')->name('integration.getConnectedAccountInfo');

Route::any('/configuration', 'ConfigurationController@index')->name('configuration');
Route::any('/save-shipment-setting', 'ConfigurationController@save_shipment_setting')->name('save-shipment-setting');
Route::post('getBpShipmentInfo', 'ConfigurationController@getBpShipmentInfo');
Route::post('refreshBpShippingMethods', 'ConfigurationController@refreshBpShippingMethods');
Route::post('refreshBpWarehouses', 'ConfigurationController@refreshBpWarehouses');
Route::post('refreshBpOrderStatus', 'ConfigurationController@refreshBpOrderStatus');
Route::post('refreshBpPriceList', 'ConfigurationController@refreshBpPriceList');

Route::post('resyncTrackingInfo', 'LogsController@resyncTrackingInfo');
Route::any('/shipment-logs/{status?}', 'LogsController@shipment_logs')->name('shipment-logs');
Route::any('/getShipmentLog', 'LogsController@getShipmentLog')->name('getShipmentLog');
//Route::post('resyncShipment', 'LogsController@resyncProduct');

/********************* end Use **********************/
// Route::any('/manage-users', 'UserController@index')->name('manage-users');
// Route::any('/user-list', 'UserController@listUser')->name('user-list');
// Route::any('/user-add', 'UserController@addUser')->name('user-add');
// Route::any('/user-delete', 'UserController@deleteUser')->name('user-delete');
// Route::any('/user-edit', 'UserController@editUser')->name('user-edit');
// Route::any('/user-update', 'UserController@updateUser')->name('user-update');
Route::any('/profile-edit', 'UserController@editProfile')->name('profile-edit');
Route::any('/profile-update', 'UserController@updateProfile')->name('profile-update');
Route::any('/password-update', 'UserController@updatePassword')->name('password-update');

Route::any('/setpassword/{token}', 'SetPasswordController@setPassword')->name('setpassword');
Route::any('/save-new-password', 'SetPasswordController@saveNewPassword')->name('save-new-password');

Route::get('/clear-cache', function () {
	$exitCode = Artisan::call('cache:clear');
	$exitCode1 = Artisan::call('view:clear');
	$exitCode2 = Artisan::call('route:clear');
	$exitCode3 = Artisan::call('config:clear');
	dd($exitCode, $exitCode1, $exitCode2, $exitCode3);
    //return redirect()->back();
})->name('clearCache');


