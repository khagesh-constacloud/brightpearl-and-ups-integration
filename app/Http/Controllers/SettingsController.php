<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use Auth;
	use DB;
	use App\Http\Controllers\BrightpearlController;
	use App\Helper\BrightpearlApi;
	use App\Helper\MainModel;

	class SettingsController extends Controller
	{

		public $obj, $BpApi, $BpCtrl;
		public function __construct(){
			$this->middleware('auth');
			$this->obj = new MainModel();
			$this->BpApi = new BrightpearlApi();
			$this->BpCtrl = new BrightpearlController();
		}
		/**
			* @return \Illuminate\Contracts\Support\Renderable
		*/

		public function index(Request $request){
			$org_id = Auth::User()->organization_id;

			$params_bp = ['api.organization_id'=>$org_id, 'api.status'=>1];
			$bp_account_details = DB::table('api_config AS api')->where($params_bp)
			->select('api.organization_id', 'api.account_name')->first();

			return view('setting',['ActiveMenu'=>'Settings','bp_account_details'=>$bp_account_details]);
		}

		public function bpOauthRedirectHandler(Request $request){
			\Storage::append('bpOauthRedirectHandler.txt', 'code: ' . print_r($request->code, true));
			if (isset($request->code)) {
				$code = $request->code;
				$client_id = env('BP_CLIENT_ID');
				$client_secret = env('BP_CLIENT_SECRET');
				$scope = 'openid offline_access ';
				$redirect_url = url('store-oauth-tokens');
				$state = $request->state;
				$state_arr = explode(':', $state);
				if (isset($state_arr[0]) && isset($state_arr[1])) { // Valid request
					//$org_id = $state_arr[1];
					$org_id = Auth::User()->organization_id;

					$AccountCode = $state_arr[0]; // Account code
					$curl_post_data = array(
						'client_id' => $client_id,
						'client_secret' => $client_secret,
						'code' => $code,
						'grant_type' => 'authorization_code',
						'redirect_uri' => $redirect_url,
						//'scope' =>$scope
					);
					$service_url = 'https://oauth.brightpearl.com/token/' . $AccountCode;
					$headers = ['Content-Type' => 'application/x-www-form-urlencoded'];
					$curl = curl_init($service_url);
					$curl_post_data = array(
						'client_id' => $client_id,
						'client_secret' => $client_secret,
						'code' => $code,
						'grant_type' => 'authorization_code',
						'redirect_uri' => $redirect_url,
						'scope' => $scope
					);
					curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
					curl_setopt($curl, CURLOPT_POST, true);
					curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
					curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($curl_post_data));
					$response = curl_exec($curl);
					curl_close($curl);
					$response = json_decode($response, true); \Storage::append('bpOauthRedirectHandler.txt', 'response: ' . print_r($response, true));
					if ($response) {
						$decode_val = $response;
						if (isset($decode_val['access_token'])) {
							$OauthData = [
								'organization_id' => $org_id,
								'account_name' => $AccountCode,
								'account_token' => base64_encode($decode_val['access_token']),
								'refresh_token' => base64_encode($decode_val['refresh_token']),
								'installation_instance_id' => $decode_val['installation_instance_id'],
								'api_domain' => $decode_val['api_domain'],
								'token_type' => $decode_val['token_type'],
								'expires_in' => $decode_val['expires_in'],
								'token_refresh_time' => time(),
								'status' => 1
							];

							$ct_account = DB::table('api_config')->where('organization_id', $org_id)->count();
							if ($ct_account > 0) {
								$op = DB::table('api_config')->where('organization_id', $org_id)->update($OauthData);
							} else {
								$op = DB::table('api_config')->insert($OauthData);
							}
							\Storage::append('bpOauthRedirectHandler.txt', 'op: ' . print_r($op, true));
							$webhook = $this->BpApi->CreateWebHooks($org_id);
							\Storage::append('bpOauthRedirectHandler.txt', 'webhook: ' . print_r($webhook, true));
						} else {
							// When Token not found
							$ct_account = DB::table('api_config')->where('organization_id', $org_id)->count();
							if ($ct_account > 0) {
								DB::table('api_config')->where('organization_id', $org_id)->update([
									'account_token' => null,
									'refresh_token' => null,
									'status' => 0
								]);
							}\Storage::append('bpOauthRedirectHandler.txt', 'token not found');
						}
						echo '<script>window.close();</script>';
					} else {
						echo 'Authentication Error<br><a href="javascript:window.close();"</a>';
					}
				}
			} else { // When code not received from BP
				echo 'Authentication Error<br><a href="javascript:window.close();"</a>';
			}
		}

		public function disconnectBrightpearlAccount(Request $request){
			$org_id = Auth::User()->organization_id;

			DB::table('api_config')->where(['organization_id'=>$org_id])
			->update(['account_name'=>null,'account_token'=>null,'refresh_token'=>null,'installation_instance_id'=>null,
			'api_domain'=>null,'token_type'=>null,'expires_in'=>null,'token_refresh_time'=>null,
			'status'=>0, 'sync_ac'=>1, 'sync_completed'=>0, 'pre_initial_sync'=>0]);

			$this->BpApi->deleteWebHooks($org_id);

			$this->obj->makeDelete('brightpearl_order',['organization_id'=>$org_id]);
			$this->obj->makeDelete('brightpearl_order_item',['organization_id'=>$org_id]);
			$this->obj->makeDelete('brightpearl_order_status',['organization_id'=>$org_id]);
			$this->obj->makeDelete('brightpearl_shipping_methods',['organization_id'=>$org_id]);
			$this->obj->makeDelete('brightpearl_warehouses',['organization_id'=>$org_id]);
			$this->obj->makeDelete('brightpearl_webhooks',['organization_id'=>$org_id]);
			$this->obj->makeDelete('commercial_invoice',['organization_id'=>$org_id]);
			$this->obj->makeDelete('commercial_invoice_line',['organization_id'=>$org_id]);
			$this->obj->makeDelete('consignee',['organization_id'=>$org_id]);
			$this->obj->makeDelete('sync_logs',['organization_id'=>$org_id]);
			$this->obj->makeDelete('sync_settings',['organization_id'=>$org_id]);
			\Cache::flush();
		}

		public function getConnectedAccountInfo(Request $request)
		{
			$org_id =  $request->organization_id;
			try {
				$ct_account = DB::table('api_config')->where('organization_id', $org_id)->first();
				if ($ct_account) {
					$ac_connected = 0;
					// $msg = "";
					if ($ct_account->status == 1 && $ct_account->account_token != null) {
						$ac_connected = 1;
						$msg = 'Account connected';
						return response()->json(['status_code' => 1, 'status_text' => $msg, 'ac_connected' => $ac_connected], 200);
					} else {
						$msg =  'Account connection failed';
						return response()->json(['status_code' => 0, 'status_text' => $msg, 'ac_connected' => $ac_connected], 200);
					}
				}
			} catch (\Exception $e) {
				return response()->json(['status_code' => 0, 'status_text' => $e->getMessage()], 200);
			}
		}

	}
