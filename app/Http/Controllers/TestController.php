<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use DB;
use App\Helper\BrightpearlApi;
use App\Http\Controllers\BrightpearlController;
use App\Helper\MainModel;

class TestController extends Controller
{
    public function test(){
        $BPC = new BrightpearlController;
        $BPA = new BrightpearlApi;
        $obj = new MainModel;

        // $account_name = 'apiworxtest8';
        // $app_ref = 'apiworxtest8_1234';
        // $account_token = 'NCE283+jNS0rpcgEiSlb2C9VzlApYY8QeIsMwzTd06Q=';
        // dd($BPC->checkBPCredentials($app_ref,$account_token,$account_name));



        //?primaryEmail={$email}&isCustomer=true&columns=contactId,firstName,lastName

        // $shopify_domain = 'm41store.myshopify.com'; length,height,width,volume
        // $access_token = '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';
        // dd($SFC->checkShopifyCredentials($shopify_domain,$access_token));

        //dd($SFC->makeUpdateShopifyItem(1, 20));
        //dd($BPA->createWebHooks(1));
        //$BPA->deleteWebHooks(1);
        //dd($BPA->GetWebhookList(1));


        //dd( $BPC->syncTrackingInformation(1, 0) );
        //dd( $BPC->getOrderDetails(1, 3608) );
        //dd( $BPA->refreshBrightpearlToken() );
        //dd( $BPC->getSOGoodOutNoteCreated(1) );
        //dd( $BPC->getGoodOutNoteBackup(1) );

        $org_id = 1;  $manual_sync_sdate = '2022-06-20'; $manual_sync_tdate = '2022-06-22'; $record_id = 70928;
        //dd( $BPC->getSOGoodOutNoteCreated($org_id, 1, $manual_sync_sdate, $manual_sync_tdate, $record_id ) );
        //dd( $BPC->getSOGoodOutNoteCreated($org_id, 1, $record_id ) );
        //dd( $BPC->getSOGoodOutNoteCreated( $org_id ) );
        //dd( $obj->getBrightpearlActiveAccounts($org_id) );
        $commercial_invoice_id = 235;
        $inv_exist = $obj->getFirstResultByConditions('commercial_invoice', ['id'=>$commercial_invoice_id], ['id']);
        dd($inv_exist);
    }
}
