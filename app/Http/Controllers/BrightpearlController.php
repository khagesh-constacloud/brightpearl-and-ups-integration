<?php
	namespace App\Http\Controllers;
	use DB;
	use App\Helper\MainModel;
	use App\Helper\BrightpearlApi;
	use App\Http\Controllers\CommonController;
	use Illuminate\Http\Request;

	class BrightpearlController extends Controller
	{
		/**
			* Create a new controller instance.
			*
			* @return void
		*/
		public static $provider_bp = "brightpearl";

		public $obj, $BpApi;
		public function __construct()
		{
			$this->BpApi = new BrightpearlApi();
			$this->log = new CommonController();
			$this->obj = new MainModel();
		}

		// This method is used to get all small and basic details of brightpearl before getting large record sets
		public function preInitialSync($user_id){
			//$this->storeProductUrls($user_id);
			$this->BpApi->storeWarehouse($user_id);
			$this->BpApi->storeOrderStatus($user_id);
			$this->BpApi->storeShippingMethods($user_id);
			$this->BpApi->storePriceList($user_id);
			$this->obj->makeUpdate('api_config', ['pre_initial_sync' => 1, 'sync_ac' => 1], ['organization_id'=>$user_id]);
		}

		// This method is used to get all large data sets. This calls after preInitialSync done.
		public function bpFetchUserInitialData($user_id){ // BP initial fetch in parts
			set_time_limit(0);

			//$this->BpApi->initialSyncExistingProductsBP($user_id);  // Product & Custom order fields metadata
			$pending_uris = 0;//$this->obj->getCountsByConditions('brightpearl_urls', ['organization_id'=>$user_id, 'status'=>0]); // Check whether all uri are processed or not. Status 0 means not processed.
			if($pending_uris == 0){
				// If no uri are pending to process then update as sync is completed
				$this->obj->makeUpdate('api_config', ['sync_completed' => 1, 'sync_ac' => 0], ['organization_id'=>$user_id]); // Updating price flag to updated
			}else{
				// If got pending uri then reset sync_ac as 1 to restart initial sync from cron
				$this->obj->makeUpdate('api_config', ['sync_ac' => 1], ['organization_id'=>$user_id]);
			}
		}

		// This methos is user to send notification to client to inform that initial sync has been done.
		public function sendAcSyncedEmail($user_id=''){
			if($user_id){
			   	$users = $this->obj->getResultByConditions('users', ['status'=>1,'id'=>$user_id], ['org_id', 'email']);
			}else{
				$users = $this->obj->getResultByConditions('users', ['status'=>1], ['org_id', 'email']);
			}

			foreach($users as $uv){
				$bp_ac = $this->obj->getFirstResultByConditions('api_config',['organization_id'=>$uv->org_id,'api_provider'=>'brightpearl','sync_completed'=>1,'status'=>1], ['id', 'mail_notified']);
				if( $bp_ac ){
					if ( $bp_ac->mail_notified=='0' ){
						$email = $uv->email;
						$final_arr = ['reason' => "Initial Sync Completed"];
						try {
							Mail::send('emails.configure_email_notification', ['data' => $final_arr], function ($msg) use ($email) {
								$msg->to($email);
								$msg->subject("Shopify-Brightpearl - Initial Sync Completed");
								$msg->from(env('MAIL_FROM_ADDRESS'), env('MAIL_FROM_NAME'));
							});
							$this->obj->makeUpdate('api_config',['mail_notified' => 1],['id'=>$bp_ac->id]);
						} catch (\Exception $e) {
							// echo $e;
						}
					}
				}
			}
		}

		/*public function storeProductUrls($org_id){
			try{
				$params = ['api_config.status'=>1];
				if($org_id){
					$params['api_config.organization_id'] = $org_id;
				}

				$bp_info = DB::table('api_config')->leftJoin('users','users.id','=','api_config.organization_id')
				->where('users.status','=',1)->where($params)->select('api_config.organization_id')
				->get();

				foreach($bp_info as $api_exsist){
					$org_id = $api_exsist->organization_id;

					$result = $this->BpApi->callCurlMethod('OPTIONS', "/product-service/product", $org_id);
					$response = json_decode($result, true);
					if(isset($response['response']['getUris'])){
						$getUris = $response['response']['getUris'];
						foreach($getUris as $getUri){
							$bp_prod_uri = $this->obj->getFirstResultByConditions('brightpearl_urls', ['url' => $getUri, 'organization_id' => $org_id, 'url_name' => 'product'], ['id']);

							if (!$bp_prod_uri) {
								$this->obj->makeInsert('brightpearl_urls', ['url' => $getUri, 'organization_id' => $org_id, 'url_name' => 'product']);
							}
						}
					}
				}
			}
			catch (\Exception $e) {
				\Log::error($e->getMessage());
				return $e->getMessage();
			}
		}*/

		/* Receive Order shipment webhook from BP */
		public function receiveShipmentWebhook(Request $request){
			\Storage::append('webhooks/'.date('Y-m-d').'/receiveShipmentWebhook.txt', "\r\n" . 'Time: ' . now());
			$organization_id = $request->org_id;

			$body = $request->getContent();

			$result_data = json_decode($body, 1);

			$mainArr = [];
			if ($result_data && isset($result_data['id'])) {
				$pids = $result_data['id'];
				$arr = explode(",", $pids);

				foreach ($arr as $val) {
					if (strpos($val, "-")) {
						$break_dash = explode("-", $val);
						$range_ids = range($break_dash[0], $break_dash[1]);
						foreach ($range_ids as $key) {
							array_push($mainArr, $key);
						}
					} else {
						array_push($mainArr, $val);
					}
				}
			}
			$mainArr = array_filter(array_unique($mainArr));
			\Storage::append('webhooks/'.date('Y-m-d').'/receiveShipmentWebhook.txt', 'mainArr: ' . print_r($mainArr, true));
			if (!empty($mainArr)) {

				/* Prepare data for insert */
				$InsertData = [];
				foreach ($mainArr as $goods_id) {
					/* Set Delete if duplicate ids found for same integration*/
					$exist = DB::table('commercial_invoice')->select('id')->where('organization_id', $organization_id)->where('invoice_number', $goods_id)->first();
					if( !$exist ){
						array_push(
							$InsertData,
							[
								'organization_id' => $organization_id,
								'invoice_number' => $goods_id,
								'shipment_sync_status' => "Pending"
							]
						);
					}
				}
				if (!empty($InsertData)) {
					$this->obj->makeInsert('commercial_invoice', $InsertData);
					\Storage::append('webhooks/'.date('Y-m-d').'/receiveShipmentWebhook.txt', 'Data inserted.');
				}
			}

		}

		/* Receive Order shipment webhook from BP */
		public function receiveShipmentDestroyedWebhook(Request $request){
			\Storage::append('webhooks/'.date('Y-m-d').'/receiveShipmentDestroyedWebhook.txt', "\r\n" . 'Time: ' . now());
			$organization_id = $request->org_id;

			$body = $request->getContent();

			$result_data = json_decode($body, 1);

			$mainArr = [];
			if ($result_data && isset($result_data['id'])) {
				$pids = $result_data['id'];
				$arr = explode(",", $pids);

				foreach ($arr as $val) {
					if (strpos($val, "-")) {
						$break_dash = explode("-", $val);
						$range_ids = range($break_dash[0], $break_dash[1]);
						foreach ($range_ids as $key) {
							array_push($mainArr, $key);
						}
					} else {
						array_push($mainArr, $val);
					}
				}
			}
			$mainArr = array_filter(array_unique($mainArr));
			\Storage::append('webhooks/'.date('Y-m-d').'/receiveShipmentDestroyedWebhook.txt', 'mainArr: ' . print_r($mainArr, true));
			if (!empty($mainArr)) {

				/* Prepare data for update */
				foreach ($mainArr as $gon_id) {
					/* Set Delete if duplicate ids found for same integration*/
					$exist = DB::table('commercial_invoice')->select('id')->where('organization_id', $organization_id)->where('invoice_number', $gon_id)->first();
					if( $exist ){
						$this->obj->makeDelete('commercial_invoice', [ 'id'=>$exist->id ]);
						\Storage::append('webhooks/'.date('Y-m-d').'/receiveShipmentDestroyedWebhook.txt', 'deleted id: ' . print_r($exist->id, true) . " | gon#: " . print_r($gon_id, true));
					}
				}

			}

		}

		/* Receive Order shipment webhook from BP */
		public function recieveSoStatusWebhook(Request $request){
			\Storage::append('webhooks/'.date('Y-m-d').'/recieveSoStatusWebhook.txt', "\r\n" . 'Time: ' . now());
			$organization_id = $request->org_id;

			$body = $request->getContent();

			$result_data = json_decode($body, 1);

			$mainArr = [];
			if ($result_data && isset($result_data['id'])) {
				$pids = $result_data['id'];
				$arr = explode(",", $pids);

				foreach ($arr as $val) {
					if (strpos($val, "-")) {
						$break_dash = explode("-", $val);
						$range_ids = range($break_dash[0], $break_dash[1]);
						foreach ($range_ids as $key) {
							array_push($mainArr, $key);
						}
					} else {
						array_push($mainArr, $val);
					}
				}
			}
			$mainArr = array_filter(array_unique($mainArr));
			\Storage::append('webhooks/'.date('Y-m-d').'/recieveSoStatusWebhook.txt', 'mainArr: ' . print_r($mainArr, true));
			if (!empty($mainArr)) {

				/* Prepare data for insert */
				foreach ($mainArr as $order_number) {
					/* Set Delete if duplicate ids found for same integration*/
					$exist = DB::table('commercial_invoice')->select('id')->where('organization_id', $organization_id)->where('order_number', $order_number)->first();
					if( $exist ){
						$this->obj->makeUpdate('commercial_invoice', [ 'shipment_sync_status'=>'Pending' ], [ 'id'=>$exist->id ]);
						\Storage::append('webhooks/'.date('Y-m-d').'/recieveSoStatusWebhook.txt', 'updated id: ' . print_r($exist->id, true) . " | order#: " . print_r($order_number, true));
					}
				}
			}

		}

		//public function getSOGoodOutNoteCreated($org_id, $manual_sync = 0, $manual_sync_sdate = null, $manual_sync_tdate = null, $record_id = 0 ){
		public function getSOGoodOutNoteCreated($org_id){
			$manual_sync = $record_id = $manual_sync_sdate = $manual_sync_tdate = 0;
			\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', "\r\n" . 'Time: ' . now() );

			$limit = 10;
			try{
				$bp_info = $this->obj->getBrightpearlActiveAccounts($org_id);

				$mappedOrderStatus = $this->getSyncConfigData($org_id, 'order_status'); // get order status filter value in this array
				$filterWarehouseIds = $this->getSyncConfigData($org_id, 'warehouse');
				$bp_default_price_list_id = $this->getSyncConfigData($org_id, 'bp_default_price_list_id');
				//$default_pirce_source = $this->getSyncConfigData($org_id, 'default_item_price_source');

				foreach($bp_info as $api_ek => $api_exsist){
					$org_id = $api_exsist->organization_id;
					$processedOrderIds = [];

					if($manual_sync){
						$query = DB::table('commercial_invoice AS inv')->select('inv.invoice_number')
						->join('commercial_invoice_line AS line', 'line.commercial_invoice_id', '=', 'inv.id')
						->where([ 'inv.organization_id'=>$org_id ]);
						if( $record_id ){
							$query->where('inv.invoice_number', $record_id);
						}else{
							//$query->where('created_at', '>=', date('Y-m-d', strtotime($manual_sync_sdate)));
							$query->where('line.updated_at', '>=', date('Y-m-d', strtotime($manual_sync_sdate)))
							->where('line.updated_at', '<=', date('Y-m-d', strtotime($manual_sync_tdate)));
						}
						$list = $query->orderBy('inv.invoice_number', 'ASC')->distinct()->limit(30)->pluck("inv.invoice_number")->toArray();
					}else{
						$list = DB::table('commercial_invoice')->select('invoice_number')
						->where([ 'organization_id'=>$org_id, 'shipment_sync_status'=> 'Pending' ])
						->orderBy('invoice_number', 'ASC')->take($limit)->pluck("invoice_number")->toArray();
					}
					\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'Pending invoice_number list: ' . print_r($list, true));

					if (!empty($list) && count($list) > 0) {
						sort($list);
						$GoodsIDs = implode(",", $list);
						\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'GoodsIDs: ' . print_r($GoodsIDs, true));

						$response = $this->getGoodsOutNotes($org_id, $GoodsIDs);

						if (!empty($response) && isset($response['response']) && is_array($response['response'])) {
							$BPOrderIDs = $UnmatchedRecord = $InvoicePrimaryIds = [];
							if(count($response['response'])>0){

								foreach ($response['response'] as $key => $value) {

									$lastId = DB::table('commercial_invoice')
									->select('id', 'invoice_number', 'order_number', 'invoice_sequence_id')
									->where([ 'organization_id'=>$org_id, 'invoice_number'=>$key ])->first();

									if ($lastId) {

										if ( isset($value['orderId']) && $value['orderId'] != 0 && isset($value['sequence']) ) {
											$processedOrderIds[] = $value['orderId'];
											if ($filterWarehouseIds) { // Check whether there are selected warehouse

												if ( !in_array($value['warehouseId'], $filterWarehouseIds) ) { // filter the data according to warehouse
													$UnmatchedRecord[] = $lastId->id;
													\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'Skipped: Unmatched warehouse: order#' . print_r($value['orderId'], true) . " | " . print_r($value['warehouseId'], true));
													continue;
												}

											}

											/* Store BP Order IDs */
											if ( isset($value['orderId']) && isset($value['sequence']) ) {
												$InvoicePrimaryIds[$lastId->id] = $value['orderId'] . "/" . $value['sequence'];
												$BPOrderIDs[$value['orderId']][] = $value['orderId'] . "/" . $value['sequence'];
											}

											$List = [
												'organization_id' => $org_id,
												'order_number' => $value['orderId'],
												'invoice_sequence_id' => $value['sequence'],
												'gon_number' => $value['orderId'] . "/" . $value['sequence'],
												'shipment_sync_status' => "Ready",
											];

											$this->obj->makeUpdate('commercial_invoice', $List, ['id'=>$lastId->id]);

											$Items = $arrProductId = $arrProductQty = [];
											if (!empty($value['orderRows'])) {

												foreach ($value['orderRows'] as $ikey => $ivalue) {

													foreach ($ivalue as $isubkey => $isubvalue) {

														//$findLine = DB::table('commercial_invoice_line')->select('id')
														//->where([ 'commercial_invoice_id'=>$lastId->id, 'orderitemid'=>$ikey, 'productId'=>$isubvalue['productId'] ])->first();

														//if (!$findLine) {
															$arrProductId[] = $isubvalue['productId'];
															$arrProductQty[ $isubvalue['productId'] ] = $isubvalue['quantity'];

															$arrKeys = $this->obj->searchForKeys( $Items, 'orderitemid', $ikey );
															if( count($arrKeys) ){
																foreach ($arrKeys as $key) {
																	$Items[$key]['quantity'] = (int)$Items[$key]['quantity'] + $isubvalue['quantity'];
																}
															}else{
																array_push($Items, [
																	'organization_id' => $org_id,
																	'orderitemid' => $ikey,
																	'productId' => $isubvalue['productId'],
																	'commercial_invoice_id' => $lastId->id,
																	'order_number' => $value['orderId'] . "/" . $value['sequence'],
																	'quantity' => $isubvalue['quantity']
																]);
															}

														//}

													}

												}
											}

											// Get SaleLine item's basic details by ID
											if( count($arrProductId) ){

												$arrProductId = array_unique($arrProductId);
												sort($arrProductId);
												$productIdSet = implode(',', $arrProductId);
												$item_response = $this->BpApi->GetProductDetails($org_id, $productIdSet);
												if (isset($item_response['response']) && !empty($item_response['response']) && is_array($item_response['response'])) {

													$products = $item_response['response'];
													foreach ($products as $key => $product) {

														$item_extra_details = $this->makeInvoiceLineData($product);
														$arrKeys = $this->obj->searchForKeys( $Items, 'productId', $product['id'] );
														if( count($arrKeys) ){
															foreach ($arrKeys as $key) {
																$Items[$key] = array_merge($Items[$key], $item_extra_details);
															}
														}

													}

												}

												/** Set item unit cost price from product price-list column */
												$price_response = $this->BpApi->GetProductPrice($org_id, $productIdSet, $bp_default_price_list_id);
												if (isset($price_response['response']) && !empty($price_response['response']) && is_array($price_response['response'])) {

													$PriceDetails = $price_response['response'];
													foreach ($PriceDetails as $key => $price) {
														$item_price_details = $this->makeInvoiceLinePriceData($price, $arrProductQty);
														$arrKeys = $this->obj->searchForKeys( $Items, 'productId', $price['productId'] );
														if( count($arrKeys) ){
															foreach ($arrKeys as $key) {
																$Items[$key] = array_merge($Items[$key], $item_price_details);
															}
														}
													}

												}

											}

											\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'Lines OK Before insert lineitems.');
											if ( count($Items) && !empty($Items)) {
												/*$invoice_line_id = $this->obj->makeInsert(
													'commercial_invoice_line',
													$Items
												);*/
												foreach ($Items as $key => $item) {
													$findLine = DB::table('commercial_invoice_line')->select('id')
													->where([ 'organization_id'=>$item['organization_id'], 'commercial_invoice_id'=>$item['commercial_invoice_id'], 'orderitemid'=>$item['orderitemid'], 'productId'=>$item['productId'] ])->first();

													if( !$findLine ){
														$invoice_line_id = $this->obj->makeInsertGetId(
															'commercial_invoice_line',
															$item
														);
														\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'Items inserted. invoice_line_id: ' . print_r($invoice_line_id, true) . "| values: ". print_r($item, true));
													}else{
														$invoice_line_id = $this->obj->makeUpdate(
															'commercial_invoice_line',
															$item,
															[ 'id'=>$findLine->id ]
														);
														\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'Items updated. invoice_line_id: ' . print_r($findLine->id, true) . "| values: ". print_r($item, true));
													}
												}
											}

										}else {
											//When order id is zero and sequence is missing
											$UnmatchedRecord[] = $lastId->id;
										}
									}
								}

							}
							/* Delete Unmatched warehouse record from shipment table */
							if (count($UnmatchedRecord) > 0) {
								\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'Deleting row that is unmatched warehouse: ' . print_r($UnmatchedRecord, true));
								DB::table('commercial_invoice')->whereIn('id', $UnmatchedRecord)->delete();
							}

							/* Retrive only array keys which is BP order ID */
							$OrderIDs = array_keys($BPOrderIDs);
							/* Unique & Sort the Array */
							$OrderIDs = array_unique($OrderIDs);
							sort($OrderIDs);
							/* Insert BP Order Details */
							$orderIds = implode(",", $OrderIDs);
							\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'orderIds to get api response: ' . print_r($orderIds, true));

							$ord_response = $this->getOrderDetails($org_id, $orderIds);

							if (isset($ord_response['response']) && !empty($ord_response['response']) && is_array($ord_response['response'])) {

								$Orders = $ord_response['response'];

								foreach ($Orders as $key => $order_details) {
									$order = $this->updateCommercialInvoiceDetails( $org_id, $order_details, $BPOrderIDs, $mappedOrderStatus, $InvoicePrimaryIds, $manual_sync );
								}

							}else{
								DB::table('commercial_invoice')->where('organization_id', $org_id)->where(
									function ($query) {
										return $query
											->where('shipment_sync_status', '=', "Pending")
											->orWhere('shipment_sync_status', '=', "Failed");
									}
								)->whereIn('invoice_number', $list)->update(['shipment_sync_status' => 'Unmatched']);

								\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'updateCommercialInvoiceDetails fail response: ' . print_r($ord_response, true));
							}
						}else{
							\Storage::append('gon_sync/'.date('Y-m-d').'/getSOGoodOutNoteCreated.txt', 'getGoodsOutNotes error: ' . print_r($response, true));
						}
					}

					\Storage::append('getSOGoodOutNoteCreated.txt', '############################# Process ends here ###########################');
					// Delete those GON id which are in DB but not returned in response via getGoodsOutNotes function
					/*if( count($processedOrderIds) && count($list) ){
						\Storage::append('getSOGoodOutNoteCreated.txt', 'processedOrderIds: ' . print_r($processedOrderIds, true));
						$result = array_diff($list, $processedOrderIds);
						$arrUntrackedGON = array_values($result);
						if( count($arrUntrackedGON) ){
							\Storage::append('getSOGoodOutNoteCreated.txt', 'processedOrderIds arrUntrackedGON: ' . print_r($arrUntrackedGON, true));
							DB::table('commercial_invoice')->whereIn( 'invoice_number', $arrUntrackedGON)->delete();
						}
					}*/
				}
			}catch (\Exception $e) {
				\Log::error($e->getMessage());
				return $e->getMessage();
			}
		}

		public function getGoodsOutNotes($org_id, $GoodsID){
			try {
				$response = $this->BpApi->getGoodsOutNotes($org_id, NULL, $GoodsID);
				if ($res = json_decode($response, true)) {
					return $res;
				}
				return false;
			} catch (\Exception $e) {
				\Log::error($e->getMessage());
				return false;
			}
		}

		/** Make product extra information for invoice line */
		public function makeInvoiceLineData($product){
			$arr_return = [];
			// Get the default unit price source from sync setting

			if( $product ){
				$arr_return['productName'] =  !empty($product['salesChannels'][0]['productName']) ? $product['salesChannels'][0]['productName'] : NULL;
				$arr_return['product_sku'] =  !empty($product['identity']['sku']) ? $product['identity']['sku'] : NULL;

				if( isset($product['stock']) && isset($product['stock']['weight']) )
					$arr_return['weight'] =  round($product['stock']['weight']['magnitude']);

				if( isset($product['stock']) && isset($product['stock']['dimensions']) ){
					$arr_return['length'] =   round($product['stock']['dimensions']['length']);
					$arr_return['height'] =   round($product['stock']['dimensions']['height']);
					$arr_return['width'] =   round($product['stock']['dimensions']['width']);
				}

				$country_of_origin = 'CN'; // China : default country of origin as per client need.
				if( isset($product['customFields']) && isset($product['customFields']['PCF_CTRYORIG']) ){
					$country_of_origin = $product['customFields']['PCF_CTRYORIG'];
				}
				$arr_return['inv_nafta_co_cn22_country_code'] = $country_of_origin;

			}

			return $arr_return;
		}

		/** Make product extra information for invoice line */
		public function makeInvoiceLinePriceData($price, $arrProductQty = []){
			$arr_return = [];
			// Get the default unit price source from sync setting

			$unit_price = 0;
			if( $price ){
				//$productId = ((!empty($price['productId'])) ? $price['productId'] : 0);
				$unit_price = ((!empty($price['priceLists'][0]['quantityPrice'][1])) ? $price['priceLists'][0]['quantityPrice'][1] : 0);
				$unit_price = number_format((float)$unit_price, 2, '.', '');
				/*$item_qty = 0;
                if( array_key_exists($productId, $arrProductQty) ){
                    $item_qty = $arrProductQty[$productId];
                }
				$total_amt = ( $item_qty * $unit_price );*/

				$arr_return['itemCost'] = $unit_price;
				//$arr_return['total_amt'] = $total_amt;
				//$arr_return['total_tax'] = 0.00;

			}

			return $arr_return;
		}

		/* Get Order In BP */
		public function getOrderDetails($org_id, $OrderID)
		{
			try {
				if( $OrderID && isset($OrderID) ){
					$response = $this->BpApi->getOrder($org_id, $OrderID);
					if ($order = json_decode($response, true)) {
						return $order;
					}
				}
				return false;
			} catch (\Exception $e) {
				\Log::error($e->getMessage());
				return false;
			}
		}

		public function updateCommercialInvoiceDetails($org_id, $order, $OrderKeyAndValues = [], $mappedOrderStatus = [], $invoicePrimaryIds = [], $manual_sync = 0 )
		{
			try{
				\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', "\r\n" . 'Time: ' . now());
				if ( is_array($order) &&  count($order) ) {

					if($order['orderTypeCode'] != 'SO'){
						DB::table('commercial_invoice')->where([ 'organization_id'=>$org_id, 'order_number'=>$order['id'] ])->delete();
						return false;
					}

					// Get the default unit price source from sync setting
					//$default_pirce_source = $this->getSyncConfigData($org_id, 'default_item_price_source');

					if (isset($OrderKeyAndValues[$order['id']])) { //check order id as key in array

						$shipmentIdsArray = $OrderKeyAndValues[$order['id']];
						if (count($shipmentIdsArray) > 0) {
							foreach ($shipmentIdsArray as $key => $shipVal) {

								$key = array_search ($shipVal, $invoicePrimaryIds);
								$commercial_invoice_id = $key ? $key : null;

								// ## Shipment filter status [start] ## //
								if( !$manual_sync ){
									$ShipmentStatus = "Ready"; $flag = true;
									if ( is_array($mappedOrderStatus) && count($mappedOrderStatus) ) { // Check whether there are selected order status
										if (!isset($order['orderStatus']['orderStatusId'])) {
											$flag = false; // continue;
										}
										if (!in_array($order['orderStatus']['orderStatusId'], $mappedOrderStatus)) {
											// filter the data according to order status if not matched then set it as Unmatched
											$flag = false;
										}
									} else {
										$ShipmentStatus = "Ready";
									}

									if( !$flag ){
										$ShipmentStatus = "Unmatched";
										// Update Shipment status if orderStatusId do not match
										$query_inv = DB::table('commercial_invoice')->where('organization_id', $org_id);
										if( $commercial_invoice_id ){
											$query_inv->where('id', $commercial_invoice_id);
										}else{
											$query_inv->where('gon_number', $shipVal);
										}
										$query_inv->update([ 'shipment_sync_status'=>$ShipmentStatus ]);

									}
								}
								// ## Shipment filter status [end] ## //

								$country_iso_code = null;
								$platform_customer_id = null;
								$fields = [];

								\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'processing GON#: ' . print_r($shipVal, true));
								/** save customer/consinee details */
								if ( !empty($order['parties']['delivery']) ) {
									if( isset($order['parties']['delivery']['email']) && !empty($order['parties']['delivery']['email']) ){
										$customer = $order['parties']['delivery'];
									}else if( isset($order['parties']['customer']['email']) && !empty($order['parties']['customer']['email']) ){
										$customer = $order['parties']['customer'];
									}else{
										$customer = $order['parties']['billing'];
									}

									$customer_email = (!@$customer['email']) ? null : trim(@$customer['email']);
									$addressFullName = (@$customer['addressFullName']) ? @$customer['addressFullName'] : null;

									$telephone = ((@$customer['telephone']) ? @$customer['telephone'] : @$customer['mobileTelephone']);
									if( isset($telephone) && !empty($telephone) ){
										$telephone = '+' . substr( preg_replace( '/\D+/', '', trim($telephone) ), 0, 11);
									}else{
										$telephone = null;
									}

									$country_iso_code = ((@$customer['countryIsoCode']) ? @$customer['countryIsoCode'] : null);
									$state_province = ((@$customer['addressLine4']) ? @$customer['addressLine4'] : null);
									if( strlen($state_province) > 2){
										$state = DB::table('states')->where([ 'name'=>$state_province ])->select('iso2')->first();
										if( $state ){
											$state_province = $state->iso2;
										}else{
											$state_province = null;
										}
									}

									$fields = array(
										'organization_id' => $org_id,
										'company_name' => ((@$customer['companyName']) ? @$customer['companyName'] : $addressFullName),
										'telephone' => $telephone,
										'notification_recp_email1' => $customer_email,
										'address1' => ((@$customer['addressLine1']) ? @$customer['addressLine1'] : null),
										'address2' =>((@$customer['addressLine2']) ? @$customer['addressLine2'] : null),
										'address3' => ((@$customer['addressLine3']) ? @$customer['addressLine3'] : null),
										'city' => ((@$customer['addressLine3']) ? @$customer['addressLine3'] : null),
										'state_province' => $state_province,
										'postal_code' => ((@$customer['postalCode']) ? @$customer['postalCode'] : null),
										'country' => $country_iso_code
									);
								}
								\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'delivery fields: ' . print_r($fields, true));

								$findCustomer = null;
								if( isset($customer_email) && $customer_email ){
									$findCustomer = $this->obj->getFirstResultByConditions('consignee', [
										'organization_id' => $org_id, 'notification_recp_email1' => $customer_email
									], ['id']);
									\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'consignee part: email' . print_r($customer_email, true). ' | findCustomer' . print_r($findCustomer, true));
								}

								if ( $findCustomer ) {

									$platform_customer_id = $findCustomer->id;
									if(count($fields)){
										$this->obj->makeUpdate('consignee', $fields, [
											'id' => $platform_customer_id
										]);
									}
									\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'consignee part: platform_customer_id' . print_r($platform_customer_id, true));

								} else {

									if(count($fields)){
										$platform_customer_id = $this->obj->makeInsertGetId('consignee', $fields);

										\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'consignee part: created platform_customer_id' . print_r($platform_customer_id, true));
									}

								}

								// Update Consignee id
								if( isset($platform_customer_id) ){
									/*$existing_inv = $this->obj->getFirstResultByConditions('commercial_invoice', [
										'organization_id'=>$org_id, 'gon_number'=>$shipVal
									], ['id']);*/
									$query2_inv = DB::table('commercial_invoice')->where('organization_id', $org_id);
									if( $commercial_invoice_id ){
										$query2_inv->where('id', $commercial_invoice_id);
									}else{
										$query2_inv->where('gon_number', $shipVal);
									}
									$existing_inv = $query2_inv->select('id')->first();

									if($existing_inv){
										$this->obj->makeUpdate('commercial_invoice',
											[ 'consignee_id' => $platform_customer_id ],
											[ 'id'=>$existing_inv->id ]
										);
										\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'consignee part: platform_customer_id: ' . print_r($platform_customer_id, true) . " | existing_inv: " . print_r($existing_inv, true));
									}
								}

								$lines = [];
								/** save order line items */
								if (!empty($order['orderRows'])) {
									$lines = $order['orderRows'];

									foreach ($lines as $line_id => $line) {

										if( count($line) ){
											\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'line item part: item found' );

											/*$existing_inv_line = $this->obj->getFirstResultByConditions('commercial_invoice_line', [
												'organization_id'=>$org_id, 'productId' => @$line['productId'], 'orderitemid'=>$line_id, 'order_number'=>$shipVal
											], ['id', 'quantity']);*/
											$query_inv_line = DB::table('commercial_invoice_line')->where([ 'organization_id'=>$org_id, 'orderitemid'=>$line_id, 'productId' => @$line['productId'] ]);
											if( $commercial_invoice_id ){
												$query_inv_line->where('commercial_invoice_id', $commercial_invoice_id);
											}else{
												$query_inv_line->where([ 'order_number'=>$shipVal ]);
											}
											$existing_inv_line = $query_inv_line->select('id', 'quantity')->first();

											\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'line item part: existing_inv_line: ' . print_r($existing_inv_line, true) );

											if($existing_inv_line){

												$array_line_item = [
													'currency_code' => ((!empty($line['rowValue']['rowNet']['currencyCode'])) ? $line['rowValue']['rowNet']['currencyCode'] : 0),
													'inv_eei_cn22_currency_code' => ((!empty($line['itemCost']['currencyCode'])) ? $line['itemCost']['currencyCode'] : 0)
												];

												$retailPrice = 0; $gon_qty = $existing_inv_line->quantity;
												if( !empty($line['rowValue']['rowNet']['value']) && (float)$line['rowValue']['rowNet']['value'] > 0 ){
													// Pricing by Order Line
													$total_amt = $line['rowValue']['rowNet']['value'];
													$array_line_item['total_amt'] = $total_amt;
													$retailPrice = ( (float)$total_amt / (int)$gon_qty);
												}else if( !empty($line['productPrice']['value']) && (float)$line['productPrice']['value'] > 0 ){
													// Pricing by List Price
													$retailPrice = $line['productPrice']['value'];
													$array_line_item['total_amt'] = ( (float)$retailPrice * (int)$gon_qty);
												}

												$array_line_item['itemRetail'] = $retailPrice;

												if( !empty($line['rowValue']['rowTax']['value']) && $line['rowValue']['rowTax']['value'] > 0 ){
													$array_line_item['total_tax'] = $line['rowValue']['rowTax']['value'];
												}

												\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'line item part: array_line_item: ' . print_r($array_line_item, true) );

												$line_result = $this->obj->makeUpdate('commercial_invoice_line', $array_line_item, [ 'id'=>$existing_inv_line->id ]);

												\Storage::append('gon_sync/'.date('Y-m-d').'/updateCommercialInvoiceDetails.txt', 'line item part: line updated: ' . print_r($line_result, true) );
											}
										}
									}
								}
							}
						}
					}
				}
			}
			catch (\Exception $e) {
				\Log::error($e->getMessage());
				return $e->getMessage();
			}
		}

		public function getSyncConfigData($org_id, $record_set){
			$return_response = false;
			if($record_set == 'warehouse'){
				$warehouse_filter_ids = DB::table('sync_settings')->select('bp_default_warehouse')
				->where(['organization_id'=>$org_id, 'allow_warehouse_filter'=>1, 'setting_type'=>'shipment'])->first();
				if( $warehouse_filter_ids ){
					$arrWarehouseFilter = explode(',', $warehouse_filter_ids->bp_default_warehouse);
				}
				$bp_warehouses = DB::table('brightpearl_warehouses')->select('warehouseId')
				->where('organization_id', $org_id)->whereIn('id', $arrWarehouseFilter)->pluck("warehouseId")->toArray();
				if( $bp_warehouses ){
					$return_response = $bp_warehouses;
				}
			}

			if($record_set == 'order_status'){
				$status_filter_ids = DB::table('sync_settings')->select('bp_default_order_status')
				->where(['organization_id'=>$org_id, 'allow_status_filter'=>1, 'setting_type'=>'shipment'])->first();
				if( $status_filter_ids ){
					$arrStatusFilter = explode(',', $status_filter_ids->bp_default_order_status);
				}
				$bp_order_status = DB::table('brightpearl_order_status')->select('statusId')
				->where('organization_id', $org_id)->whereIn('id', $arrStatusFilter)->pluck("statusId")->toArray();
				if( $bp_order_status ){
					$return_response = $bp_order_status;
				}
			}

			if($record_set == 'shipping_method'){
				$result = DB::table('sync_settings AS setting')->select('ship_method.shipment_method_id')
				->join('brightpearl_shipping_methods AS ship_method', 'ship_method.id', '=', 'setting.bp_default_shipping_method')
				->where(['setting.organization_id'=>$org_id, 'setting.setting_type'=>'shipment'])->first();
				if( $result ){
					$return_response = $result->shipment_method_id;
				}
			}

			if($record_set == 'default_item_price_source'){
				$result = DB::table('sync_settings AS setting')->select('setting.default_item_price_source')
				->where(['setting.organization_id'=>$org_id, 'setting.setting_type'=>'shipment'])->first();
				if( $result ){
					$return_response = $result->default_item_price_source;
				}
			}

			if($record_set == 'bp_default_price_list_id'){
				$result = DB::table('sync_settings AS setting')
				->join('brightpearl_price_list AS price_list', 'price_list.id', '=', 'setting.bp_default_price_list_id')
				->select('price_list.price_list_id')
				->where(['setting.organization_id'=>$org_id, 'setting.setting_type'=>'shipment'])->first();
				if( $result ){
					$return_response = $result->price_list_id;
				}
			}

			return $return_response;
		}

		public function syncTrackingInformation($org_id, $RecordID)
		{
			$return_response = false;
			\Storage::append('trackingInfo/'.date('Y-m-d').'/syncTrackingInformation.txt', "\r\n" . 'Time: ' . now());
			try {
				$limit = 10;
				$bp_info = $this->obj->getBrightpearlActiveAccounts($org_id);
				if ( count($bp_info) ) {
					$query = DB::table('commercial_invoice')->select('id', 'order_number', 'invoice_number', 'invoice_sequence_id', 'shipment_sync_status', 'tracking_number');
					if ($RecordID && $RecordID !== 0) {
						$query->where('id', $RecordID);
					} else {
						$query->where(['organization_id'=>$org_id, 'tracking_sync_status'=>'Ready' ])
						->whereNotNull('tracking_number');
					}
					$list = $query->orderBy('order_number', 'ASC')->take($limit)->get();
					\Storage::append('trackingInfo/'.date('Y-m-d').'/syncTrackingInformation.txt', 'list: ' . print_r($list, true));

					if (!empty($list) && count($list) > 0) {
						$shippingId = $this->getSyncConfigData($org_id, 'shipping_method');

						$postArr = [];
						foreach ($list as $key => $shipment) {
							if ( isset($shipment->invoice_number) && isset($shipment->tracking_number)) {
								$postArr['priority'] = false;
								if ($shippingId) {
									$postArr['shipping']["shippingMethodId"] = $shippingId;
								}
								if ($shipment->tracking_number) {
									$postArr['shipping']["reference"] = (strlen($shipment->tracking_number) > 64) ? substr($shipment->tracking_number, 0, 64) : $shipment->tracking_number;
								}
								$postArr['shipping']["boxes"] = null;
								$postArr['shipping']["weight"] = null;
								$postArr['labelUri'] = null;
								/*if ($shipment->boxes) {
									$postArr['shipping']["boxes"] = $shipment->boxes;
								}
								if ($shipment->weight) {
									$postArr['shipping']["weight"] = $shipment->weight;
								}
								if ($shipment->tracking_url) {
									$postArr['labelUri'] = (strlen($shipment->tracking_url) > 512) ? substr($shipment->tracking_url, 0, 512) : $shipment->tracking_url;
								}*/

								$response = $this->BpApi->putGoodsOutNotes(
									$org_id,
									NULL,
									$postArr,
									$shipment->invoice_number
								);

								$response_data = json_decode($response, true);

								if (isset($response_data) && empty($response_data)) {
									$response = $this->BpApi->postGoodsOutNoteEvent($org_id, NULL, [
										"events" => [
											[
												"eventCode" => "SHW",
												"occured" => date(DATE_ISO8601),
												"eventOwnerId" => rand(2, 2)
											]
										]
									], $shipment->invoice_number);
									$eventResponse = json_decode($response, true);
									if (isset($eventResponse) && empty($eventResponse)) {
										DB::table('commercial_invoice')->where(['organization_id'=>$org_id, 'id'=>$shipment->id])->update(['tracking_sync_status'=>"Synced"]);
										DB::table('brightpearl_order')->where(['organization_id'=>$org_id, 'sales_order_id'=>$shipment->order_number])->update(['shipment_sync_status'=>"Synced"]);
										$msg = 'Tracking information updated successfully';
										$this->log->sync_log($org_id, $shipment->id, 'tracking_info', 'synced', $msg);
									}
									else if (isset($eventResponse['errors']) || isset($eventResponse['response'])) {
										DB::table('commercial_invoice')->where(['organization_id'=>$org_id, 'id'=>$shipment->id])->update(['tracking_sync_status'=>"Failed"]);
										DB::table('brightpearl_order')->where(['organization_id'=>$org_id, 'sales_order_id'=>$shipment->order_number])->update(['shipment_sync_status'=>"Failed"]);
										$error = $this->BpApi->handleResponseError($eventResponse);
										/* Customize BP Errors */
										$GON = $shipment->order_number . "/" . $shipment->invoice_sequence_id;
										$response = $this->BpApi->MakeCustomError($error, $GON);
										if ($response['status'] == 1) {
											$error = $response['status_text'];
										}
										/* ------------------- */

										$this->log->sync_log($org_id, $shipment->id, 'tracking_info', 'failed', $error);
									}
								} else if (isset($response_data['errors']) || isset($response_data['response'])) {
									DB::table('commercial_invoice')->where(['organization_id'=>$org_id, 'id'=>$shipment->id])->update(['tracking_sync_status'=>"Failed"]);
									DB::table('brightpearl_order')->where(['organization_id'=>$org_id, 'sales_order_id'=>$shipment->order_number])->update(['shipment_sync_status'=>"Failed"]);
									$error = $this->BpApi->handleResponseError($response_data);
									/* Customize BP Errors */
									$GON = $shipment->order_number . "/" . $shipment->invoice_sequence_id;
									$response = $this->BpApi->MakeCustomError($error, $GON);
									if ($response['status'] == 1) {
										$error = $response['status_text'];
									}
									/* ------------------- */

									$this->log->sync_log($org_id, $shipment->id, 'tracking_info', 'failed', $error);
								}
							}
						}
						$return_response = true;
					}
				}
			} catch (\Exception $e) {
				$return_response = $e->getMessage();
			}
			\Storage::append('trackingInfo/'.date('Y-m-d').'/syncTrackingInformation.txt', 'response: ' . print_r($return_response, true));
			return $return_response;
		}

		public function ResetSaleLineDetails($org_id){
			\Storage::append('reset_saleline/'.date('Y-m-d').'/ResetSaleLineDetails.txt', "\r\n" . 'Time: ' . now());
			$return = false;
			do{
				$allow_next_call = false;
				$limit = 20;

				$order_details = DB::table('commercial_invoice AS inv')->leftJoin('commercial_invoice_line AS inv_line', 'inv_line.commercial_invoice_id', '=', 'inv.id')
				->select('inv.id', 'inv.order_number', 'inv.invoice_sequence_id')
				->whereNotNull('inv.order_number')->whereNull('inv_line.order_number')->orderBy('inv.updated_at', 'ASC')->limit($limit)->get();

				$order_numbers = $arrOrderIds = $arrOrderSeqIds = [];
				foreach ($order_details as $key => $value) {
					$order_numbers[] = $value->order_number;
					$arrOrderIds[$value->order_number] = $value->id;
					$arrOrderSeqIds[$value->order_number] = $value->invoice_sequence_id;
				}

				$order_numbers = array_unique($order_numbers);
				sort($order_numbers);
				$orderIds = implode(",", $order_numbers);
				\Storage::append('reset_saleline/'.date('Y-m-d').'/ResetSaleLineDetails.txt', 'orderIds: ' . print_r($orderIds, true));

				$result = $this->getOrderDetails($org_id, $orderIds);
				if ( $result && isset($result['response']) ) {
					$Orders = $result['response'];
					$i=0;
					foreach ($Orders as $order) {
						\Storage::append('reset_saleline/'.date('Y-m-d').'/ResetSaleLineDetails.txt', '----- order#: ' . print_r($order['id'], true));
						$i+=1;
						if ( !empty($order['parties']['delivery']) ) {
							$customer = $order['parties']['delivery'];
							$country_iso_code = ((@$customer['countryIsoCode']) ? @$customer['countryIsoCode'] : null);
						}

						$commercial_invoice_id = $sequence_id = null;
						if( array_key_exists($order['id'], $arrOrderIds) ){
							$commercial_invoice_id = $arrOrderIds[$order['id']];
						}
						if( array_key_exists($order['id'], $arrOrderSeqIds) ){
							$sequence_id = $arrOrderSeqIds[$order['id']];
						}

						if (!empty($order['orderRows'])) {
							$lines = $order['orderRows'];
							foreach ($lines as $line_id => $line) {
								$Items = array(
									'organization_id' => $org_id,
									'commercial_invoice_id' => $commercial_invoice_id,
									'orderitemid' => $line_id,
									'order_number' => $order['id'] . '/' . $sequence_id,
									'productId' => @$line['productId'],
									'productName' => @$line['productName'],
									'product_sku' => @$line['productSku'],
									'quantity' => ((!empty($line['quantity']['magnitude'])) ? $line['quantity']['magnitude'] : 0),
									'itemCost' => ((!empty($line['itemCost']['value'])) ? $line['itemCost']['value'] : 0),
									'total_amt' => ((!empty($line['rowValue']['rowNet']['value'])) ? $line['rowValue']['rowNet']['value'] : 0),
									'total_tax' => ((!empty($line['rowValue']['rowTax']['value'])) ? $line['rowValue']['rowTax']['value'] : 0),
									'currency_code' => ((!empty($line['rowValue']['rowNet']['currencyCode'])) ? $line['rowValue']['rowNet']['currencyCode'] : 0),
									'inv_eei_cn22_currency_code' => ((!empty($line['itemCost']['currencyCode'])) ? $line['itemCost']['currencyCode'] : 0),
									'inv_nafta_co_cn22_country_code' => 'CN'
								);
								\Storage::append('reset_saleline/'.date('Y-m-d').'/ResetSaleLineDetails.txt', 'Items before merge: ' . print_r($Items, true));

								$item_response = $this->BpApi->GetProductDetails($org_id, @$line['productId']);
								if (isset($item_response['response']) && !empty($item_response['response']) && is_array($item_response['response'])) {
									$products = $item_response['response'];
									foreach ($products as $key => $product) {
										$item_extra_details = $this->makeInvoiceLineData($product);
										$Items = array_merge($Items, $item_extra_details);
										\Storage::append('reset_saleline/'.date('Y-m-d').'/ResetSaleLineDetails.txt', 'Items after merge: ' . print_r($Items, true));

										$inv_exist = $this->obj->getFirstResultByConditions('commercial_invoice', ['id'=>$commercial_invoice_id], ['id']);
										if ( $inv_exist && !empty($Items)) {
											$line_id = $this->obj->makeInsertGetId('commercial_invoice_line', $Items);
											\Storage::append('reset_saleline/'.date('Y-m-d').'/ResetSaleLineDetails.txt', 'updated commercial_invoice_line' . print_r($line_id, true));
										}
									}
								}
							}
						}
					}
					$return = true;
				}else{
					$return = false;
				}

				/*if( $i < $limit){
					$allow_next_call = false;
				}*/

			}while($allow_next_call);
			return $return;
		}

		public function getGoodOutNoteBackup($org_id){
			$return_response = false;
			\Storage::append('gon_backupCall/'.date('Y-m-d').'/getGoodOutNoteBackup.txt', "\r\n" . 'Time: '. now());
			try{
				$bp_info = $this->obj->getBrightpearlActiveAccounts($org_id);

				if( $bp_info && count($bp_info) ){
					foreach($bp_info as $api_ek => $api_exsist){
						$org_id = $api_exsist->organization_id;

						$check_urls = DB::table('sync_url')->where([ 'organization_id'=>$org_id, 'url_name' => 'goodsout_lasttime' ])->select('url', 'id')->first();
						/* Find last Goods Out Note time from DB */
                        if ($check_urls) {
                            /* If GON last time found */
                            $sync_start_date = trim($check_urls->url);
                        } else {
                            /* If GON last time not found  | set 1 hr minus from current time*/
                            $sync_start_date = date(DATE_ISO8601, strtotime('-1 hour'));
                        }

						$end_created_on_date = $sync_start_date;
                        $sync_start_date = str_replace('+', '%2B', $sync_start_date);
                        $end_date = str_replace('+', '%2B', date(DATE_ISO8601));
						$process_limit = 100;

						$url = "/warehouse-service/goods-note/goods-out-search?columns=goodsOutNoteId,createdOn&pageSize=" . $process_limit . "&createdOn=" . $sync_start_date . "/" . $end_date; // &sort=createdOn|ASC
						\Storage::append('gon_backupCall/'.date('Y-m-d').'/getGoodOutNoteBackup.txt', 'url: '. print_r($url, true));

						$response = $this->BpApi->getGoodsOutNotes($org_id, $url, null);
						if ($bsgoods = json_decode($response, true) ) {

                            if (!empty($bsgoods) && isset($bsgoods['response']['results']) && is_array($bsgoods['response']['results'])) {
                                $mainResult = $bsgoods['response']['results'];

                                foreach ($mainResult as $pkey => $shipment) {
                                    if (isset($shipment[0])) {
										$find = DB::table('commercial_invoice')->where([ 'organization_id'=>$org_id, 'invoice_number'=>$shipment[0] ])->first();

                                        if (!$find) {
											$this->obj->makeInsert('commercial_invoice', [
												'organization_id' => $org_id,
												'invoice_number' => $shipment[0],
												'shipment_sync_status' => "Pending"
											]);
											\Storage::append('gon_backupCall/'.date('Y-m-d').'/getGoodOutNoteBackup.txt', 'added missed gon: '. print_r($shipment[0], true));
                                        }
                                        $end_created_on_date = $shipment[1];
                                    }
                                }

                                if ($mainResult) {

                                    if ($check_urls) {
                                        //Update last goods out note created time
										$this->obj->makeUpdate('sync_url', [ 'url' => $end_created_on_date ], [ 'id'=>$check_urls->id ]);
                                    } else {
                                        //insert last goods out note created time
                                        $this->obj->makeInsert('sync_url', [
                                            'organization_id' => $org_id,
                                            'url' => $end_created_on_date,
                                            'url_name' => "goodsout_lasttime"
                                        ]);

                                    }
                                }
								$return_response = true;
                            } else {
								$return_response = $this->BpApi->handleResponseError($bsgoods);
                            }
                        }
					}
				}

			}catch (\Exception $e) {
				$return_response = $e->getMessage();
			}
			\Storage::append('gon_backupCall/'.date('Y-m-d').'/getGoodOutNoteBackup.txt', 'response: '. print_r($return_response, true));
			return $return_response;
		}

	}
