<?php

	namespace App\Http\Controllers;

	use Illuminate\Http\Request;
	use Auth;
	use DB;
	use App\Http\Controllers\BrightpearlController;
	use Yajra\Datatables\Datatables;

	class LogsController extends Controller
	{

		/**
			* @return \Illuminate\Contracts\Support\Renderable
		*/

		public function __construct()
		{
			$this->middleware('auth');
		}

		public function shipment_logs(Request $request){
			return view('shipment_log',['ActiveMenu'=>'Shipment Logs']);
		}

		public function getShipmentLog(Request $request){
			$org_id = Auth::user()->organization_id;
			$filter_by_status = $request->filter_by_status;

			$Results = DB::table('commercial_invoice As inv')
			->select('inv.id', 'inv.order_number', 'inv.invoice_number', 'inv.gon_number', 'inv.tracking_number', 'inv.shipment_sync_status', 'inv.tracking_sync_status', 'inv.updated_at')
			->where([ 'inv.organization_id'=>$org_id, 'inv.is_deleted'=>0 ])
			->Where(function ($query) use($filter_by_status) {
				if($filter_by_status=='synced'){
					$query->where('inv.shipment_sync_status','Synced');
				}else if($filter_by_status=='pending'){
					$query->where('inv.shipment_sync_status','Pending')
					->orWhere('inv.shipment_sync_status','Ready');
				}else if($filter_by_status=='failed'){
					$query->where('inv.shipment_sync_status','Failed');
				}else if($filter_by_status=='unmatched'){
					$query->where('inv.shipment_sync_status','Unmatched');
				}
			})->get();

			foreach($Results as $result)
			{
				$result->updated_at = $result->updated_at ? date('d-m-Y h:i A',strtotime($result->updated_at)) : '';
				$result->tracking_number = $result->tracking_number ? $result->tracking_number : 'N/A';
				//$result->sync_date = date('m/d/Y h:i A',strtotime($result->sync_date) - (5*60*60));
				//$result->product_sync_status = strtoupper($result->product_sync_status);
			}
			return Datatables::of($Results)->make(true);
		}

		public function resyncTrackingInfo(Request $request){
			$BpCtrl = new BrightpearlController;
			$id = $request->id;
			dd( $id  );
			$response = response()->json(['status_code' => 0, 'status_text' => 'Error In Resync. Please try again']);

			$nresponse = $BpCtrl->syncTrackingInformation(Auth::user()->id, $id);

			if($nresponse){
				return $nresponse;
			}else{
				$response = response()->json(['status_code' => 0, 'status_text' => 'Error in Resync. Please check your Sync Configuration Settings']);
			}

			return $response;
		}

	}
