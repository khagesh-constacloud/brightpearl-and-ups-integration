<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $org_id = Auth::User()->organization_id;

        $shipment_total = DB::table('commercial_invoice')->where('organization_id',$org_id)->count();
        $shipment_pending = DB::table('commercial_invoice')->where('organization_id',$org_id)->whereIn('shipment_sync_status',['Pending','Ready'])->count();
		$shipment_synced = DB::table('commercial_invoice')->where('organization_id',$org_id)->whereIn('shipment_sync_status',['Synced'])->count();
		$shipment_failed = DB::table('commercial_invoice')->where('organization_id',$org_id)->where('shipment_sync_status','Failed')->count();

		return view('home',['ActiveMenu'=>'Dashboard','shipment_total'=>$shipment_total,'shipment_pending'=>$shipment_pending,'shipment_synced'=>$shipment_synced,'shipment_failed'=>$shipment_failed]);
    }

}
