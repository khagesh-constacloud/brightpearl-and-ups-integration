<?php
	namespace App\Http\Controllers;
	use Illuminate\Http\Request;
	use Auth;
	use DB;
	use App\Helper\MainModel;
	use App\Helper\BrightpearlApi;

	class ConfigurationController extends Controller
	{
		/**
			* Create a new controller instance.
			*
			* @return void
		*/

		public $obj, $BpApi;
		public function __construct()
		{
			$this->middleware('auth');
			$this->obj = new MainModel();
			$this->BpApi = new BrightpearlApi();
		}

		public function index(Request $request)
		{
			$org_id = Auth::User()->organization_id;

			$params_bp = ['api.organization_id'=>$org_id, 'api.status'=>1];
			$bp_account = DB::table('api_config AS api')->where($params_bp)
			->select('api.organization_id', 'api.account_name', 'api.account_token', 'api.status')->first();

			$shipment = $this->obj->getFirstResultByConditions('sync_settings',
			['organization_id'=>$org_id, 'setting_type'=>'shipment'],
			['allow_warehouse_filter', 'bp_default_warehouse', 'allow_status_filter', 'bp_default_order_status', 'default_item_price_source']);

			$ActiveMenu = 'Sync Configuration';
			if(isset($bp_account->account_token)){
				return view('sync_configuration', compact('ActiveMenu','bp_account','shipment'));
			}else{
				return redirect()->route('settings');
			}

		}

		public function getBpShipmentInfo(Request $request){
			$warehouse_opt = ''; $order_status_opt = ''; $shipping_method_opt = $price_list_opt = '';

			$bp_order_status = $this->obj->getResultByConditions('brightpearl_order_status',['organization_id'=>Auth::user()->organization_id, 'disabled'=>0, 'orderTypeCode'=>'SO']);
			$bp_warehouse = $this->obj->getResultByConditions('brightpearl_warehouses',['organization_id'=>Auth::user()->organization_id]);
			$bp_shipping_method = $this->obj->getResultByConditions('brightpearl_shipping_methods',['organization_id'=>Auth::user()->organization_id]);
			$bp_price_list = $this->obj->getResultByConditions('brightpearl_price_list',['organization_id'=>Auth::user()->organization_id]);

			$shipment = $this->obj->getFirstResultByConditions('sync_settings',
			['organization_id'=>Auth::user()->organization_id, 'setting_type'=>'shipment'],
			['allow_warehouse_filter', 'bp_default_warehouse', 'allow_status_filter', 'bp_default_order_status', 'bp_default_shipping_method', 'bp_default_price_list_id']);

			$arrWarehouseFilters = [];
			if( $shipment && $shipment->bp_default_warehouse ){
				$arrWarehouseFilters = explode(',', $shipment->bp_default_warehouse);
			}

			foreach($bp_warehouse as $fk => $fv){
				$select_opt = '';
				for($i=0; $i<count($arrWarehouseFilters); $i++){
					if($shipment && $arrWarehouseFilters[$i] == $fv->id ){
						$select_opt = 'selected';
					}
				}
				$warehouse_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			$arrStatusFilters = [];
			if( $shipment && $shipment->bp_default_order_status ){
				$arrStatusFilters = explode(',', $shipment->bp_default_order_status);
			}

			foreach($bp_order_status as $fk => $fv){
				$select_opt = '';
				for($i=0; $i<count($arrStatusFilters); $i++){
					if($shipment && $arrStatusFilters[$i] == $fv->id ){
						$select_opt = 'selected';
					}
				}
				$order_status_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			foreach($bp_shipping_method as $fk => $fv){
				$select_opt = '';
				if($shipment && $shipment->bp_default_shipping_method == $fv->id ){
					$select_opt = 'selected';
				}
				$shipping_method_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			foreach($bp_price_list as $fk => $fv){
				$select_opt = '';
				if($shipment && $shipment->bp_default_price_list_id == $fv->id ){
					$select_opt = 'selected';
				}
				$price_list_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			return response()->json(['status_code' => 1, 'status_text' => 'Sales fields updated successfully','warehouse_opt'=>$warehouse_opt
			,'order_status_opt'=>$order_status_opt,'shipping_method_opt'=>$shipping_method_opt,'price_list_opt'=>$price_list_opt]);
		}

		public function refreshBpShipmentValues(Request $request){ // sync configuration refresh call
			$this->BpApi->storeWarehouse(Auth::user()->organization_id);
			$this->BpApi->storeOrderStatus(Auth::user()->organization_id);

			$request_new = new \Illuminate\Http\Request();
			$request_new->replace([]);

			$res = $this->getBpShipmentInfo($request_new);

			return $res;
		}

		public function save_shipment_setting(Request $request)
		{
			$org_id = Auth::User()->organization_id;

			$bp_warehouse_filters = null;
			if(isset($request->bp_default_warehouse) ){
				$warehouses = implode(",", $request->bp_default_warehouse);
				$bp_warehouse_filters = $warehouses;
			}
			$bp_status_filters = null;
			if(isset($request->bp_default_order_status) ){
				$order_status = implode(",", $request->bp_default_order_status);
				$bp_status_filters = $order_status;
			}
			if(!$request->allow_warehouse_filter){
				$bp_warehouse_filters = null;
			}
			if(!$request->allow_status_filter){
				$bp_status_filters = null;
			}

			$fields = array(
				'allow_warehouse_filter' => @$request->allow_warehouse_filter ? $request->allow_warehouse_filter : 0,
				'allow_status_filter' => @$request->allow_status_filter ? $request->allow_status_filter : 0,
				'bp_default_warehouse' => $bp_warehouse_filters ? $bp_warehouse_filters : null,
				'bp_default_order_status' => $bp_status_filters ? $bp_status_filters : null,
				'bp_default_shipping_method' => @$request->bp_default_shipping_method ? @$request->bp_default_shipping_method : null,
				'default_item_price_source' => @$request->default_item_price_source ? @$request->default_item_price_source : null,
				'bp_default_price_list_id' => @$request->bp_default_price_list_id ? @$request->bp_default_price_list_id : null
			);

			$sync_settings = $this->obj->getFirstResultByConditions('sync_settings',['organization_id'=>$org_id], ['id']);
			if( $sync_settings ){
				$this->obj->makeUpdate('sync_settings', $fields, [ 'id'=>$sync_settings->id ]);
			}else{
				$fields['organization_id'] = $org_id;
				$this->obj->makeInsert('sync_settings', $fields);
			}

			return back()->with('success', 'Settings are updated!');
		}

		public function getBpShippingMethods(Request $request){
			$shipping_method_opt = '';

			$bp_shipping_method = $this->obj->getResultByConditions('brightpearl_shipping_methods',['organization_id'=>Auth::user()->organization_id]);

			$shipment = $this->obj->getFirstResultByConditions('sync_settings',
			['organization_id'=>Auth::user()->organization_id, 'setting_type'=>'shipment'],
			['bp_default_shipping_method']);

			foreach($bp_shipping_method as $fk => $fv){
				$select_opt = '';
				if($shipment && $shipment->bp_default_shipping_method == $fv->id ){
					$select_opt = 'selected';
				}
				$shipping_method_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			return response()->json(['status_code' => 1, 'status_text' => 'Brightpearl shipping methods are updated successfully', 'shipping_method_opt'=>$shipping_method_opt]);
		}

		public function getBpPriceList(Request $request){
			$price_list_opt = '';

			$bp_price_list = $this->obj->getResultByConditions('brightpearl_price_list',['organization_id'=>Auth::user()->organization_id]);

			$shipment = $this->obj->getFirstResultByConditions('sync_settings',
			['organization_id'=>Auth::user()->organization_id, 'setting_type'=>'shipment'],
			['bp_default_price_list_id']);

			foreach($bp_price_list as $fk => $fv){
				$select_opt = '';
				if($shipment && $shipment->bp_default_price_list_id == $fv->id ){
					$select_opt = 'selected';
				}
				$price_list_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			return response()->json(['status_code' => 1, 'status_text' => 'Brightpearl price list are updated successfully', 'price_list_opt'=>$price_list_opt]);
		}

		public function refreshBpShippingMethods(Request $request){ // sync configuration refresh call
			$this->BpApi->storeShippingMethods(Auth::user()->organization_id);
			$request_new = new \Illuminate\Http\Request();
			$request_new->replace([]);
			$res = $this->getBpShippingMethods($request_new);
			return $res;
		}

		public function refreshBpPriceList(Request $request){ // sync configuration refresh call
			$this->BpApi->storePriceList(Auth::user()->organization_id);
			$request_new = new \Illuminate\Http\Request();
			$request_new->replace([]);
			$res = $this->getBpPriceList($request_new);
			return $res;
		}

		public function getBpWarehouses(Request $request){
			$warehouse_opt = '';

			$bp_warehouse = $this->obj->getResultByConditions('brightpearl_warehouses',['organization_id'=>Auth::user()->organization_id]);

			$shipment = $this->obj->getFirstResultByConditions('sync_settings',
			['organization_id'=>Auth::user()->organization_id, 'setting_type'=>'shipment'],
			['allow_warehouse_filter', 'bp_default_warehouse']);

			$arrWarehouseFilters = [];
			if( $shipment && $shipment->bp_default_warehouse ){
				$arrWarehouseFilters = explode(',', $shipment->bp_default_warehouse);
			}

			foreach($bp_warehouse as $fk => $fv){
				$select_opt = '';
				for($i=0; $i<count($arrWarehouseFilters); $i++){
					if($shipment && $arrWarehouseFilters[$i] == $fv->id ){
						$select_opt = 'selected';
					}
				}
				$warehouse_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			return response()->json(['status_code' => 1, 'status_text' => 'Brightpearl warehouses are updated successfully', 'warehouse_opt'=>$warehouse_opt]);
		}

		public function refreshBpWarehouses(Request $request){ // sync configuration refresh call
			$this->BpApi->storeWarehouse(Auth::user()->organization_id);
			$request_new = new \Illuminate\Http\Request();
			$request_new->replace([]);
			$res = $this->getBpWarehouses($request_new);
			return $res;
		}

		public function getOrderStatus(Request $request){
			$order_status_opt = '';

			$bp_order_status = $this->obj->getResultByConditions('brightpearl_order_status',['organization_id'=>Auth::user()->organization_id, 'disabled'=>0, 'orderTypeCode'=>'SO']);

			$shipment = $this->obj->getFirstResultByConditions('sync_settings',
			['organization_id'=>Auth::user()->organization_id, 'setting_type'=>'shipment'],
			['allow_status_filter', 'bp_default_order_status']);

			$arrStatusFilters = [];
			if( $shipment && $shipment->bp_default_order_status ){
				$arrStatusFilters = explode(',', $shipment->bp_default_order_status);
			}

			foreach($bp_order_status as $fk => $fv){
				$select_opt = '';
				for($i=0; $i<count($arrStatusFilters); $i++){
					if($shipment && $arrStatusFilters[$i] == $fv->id ){
						$select_opt = 'selected';
					}
				}
				$order_status_opt .= '<option '.$select_opt.' value="'.$fv->id.'">'.$fv->name.'</option>';
			}

			return response()->json(['status_code' => 1, 'status_text' => 'Brightpearl order status are updated successfully', 'order_status_opt'=>$order_status_opt,]);
		}

		public function refreshBpOrderStatus(Request $request){ // sync configuration refresh call
			$this->BpApi->storeOrderStatus(Auth::user()->organization_id);
			$request_new = new \Illuminate\Http\Request();
			$request_new->replace([]);
			$res = $this->getOrderStatus($request_new);
			return $res;
		}

	}
