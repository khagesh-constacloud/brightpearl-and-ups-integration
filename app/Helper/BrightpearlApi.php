<?php

namespace App\Helper;

use Auth;
use DB;
use App\BrightpearlSync1;
use App\Helper\MainModel;
use App\Common;
use App\Settings;
use Carbon\Carbon;

class BrightpearlApi
{
    public $obj, $common;
    public function __construct(){
        //$this->common = new Common();
        $this->obj = new MainModel();
    }

    /* Common Method Brightpeal Curl Method */
    public function callCurlMethod_test($methodType = '', $url = '', $accountCode = '', $body = '')
    {
        $result = '';
        $ch = curl_init();

        $auth = 1;

        if ($auth) {
            //if (isset($auth->account_name)) {
                //  $account_name = 'apiworxtest8';
                //  $app_ref = 'apiworxtest8_gems';
                //  $access_token = 'rqBup7NqQu5QWoND9HxBbqaI4V251VTIzb0qx7X7sFE=';

                // $account_name = 'bellsofsteelnew';
                // $app_ref = 'bellsofsteelnew_0001';
                // $access_token = 'ZZbDfbmZu4/YFpF65Uoc6aLJD4+1D6AsULn/x0r/tzU=';

                $account_name = 'bellsofsteelnew';
                $app_ref = 'bellsofsteelnew_0001';
                $access_token = 'ZZbDfbmZu4/YFpF65Uoc6aLJD4+1D6AsULn/x0r/tzU=';

                $baseUrl = env('BP_API_BASE_URL') . $account_name . $url;

                if (strtolower($methodType) == 'post') {
                    /* CURL POST METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'brightpearl-app-ref:' . $app_ref;
                    $headers[] = 'brightpearl-account-token:' . $access_token;


                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    // sleep(1);
                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'put') {
                    /* CURL POST METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'brightpearl-app-ref:' . $app_ref;
                    $headers[] = 'brightpearl-account-token:' . $access_token;

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    // sleep(1);
                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'patch') {
                    /* CURL POST METHOD */

                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'brightpearl-app-ref:' . $app_ref;
                    $headers[] = 'brightpearl-account-token:' . $access_token;


                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    // sleep(1);
                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'delete') {
                    /* CURL GET METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'brightpearl-app-ref:' . $app_ref;
                    $headers[] = 'brightpearl-account-token:' . $access_token;


                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    // sleep(1);
                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'get') {
                    /* CURL GET METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'brightpearl-app-ref:' . $app_ref;
                    $headers[] = 'brightpearl-account-token:' . $access_token;

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    // sleep(1);
                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else {
                    /* CURL GET METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'brightpearl-app-ref:' . $app_ref;
                    $headers[] = 'brightpearl-account-token:' . $access_token;

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    // sleep(1);
                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                }
            //}
        }

        return $result;
    }

    /* Common Method Brightpeal Curl Method */
    public function callCurlMethod($methodType = '', $url = '', $accountCode = '', $body = ''){
        $result = '';
        $ch = curl_init();

        $auth = $this->getBPCredentials($accountCode);

        if ($auth) {
            if (isset($auth->account_name)) {
                $baseUrl = env('BP_API_BASE_URL') . $auth->account_name . $url;
                $access_token = base64_decode($auth->account_token);

                if (strtolower($methodType) == 'post') {
                    /* CURL POST METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'Authorization: Bearer ' . $access_token;
                    $headers[] = 'brightpearl-dev-ref:' . env('BP_DEV_REF');
                    $headers[] = 'brightpearl-app-ref:' . env('BP_CLIENT_ID');

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'put') {
                    /* CURL POST METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'Authorization: Bearer ' . $access_token;
                    $headers[] = 'brightpearl-dev-ref:' . env('BP_DEV_REF');
                    $headers[] = 'brightpearl-app-ref:' . env('BP_CLIENT_ID');

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'patch') {
                    /* CURL POST METHOD */

                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'Authorization: Bearer ' . $access_token;
                    $headers[] = 'brightpearl-dev-ref:' . env('BP_DEV_REF');
                    $headers[] = 'brightpearl-app-ref:' . env('BP_CLIENT_ID');

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'delete') {
                    /* CURL GET METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'Authorization: Bearer ' . $access_token;
                    $headers[] = 'brightpearl-dev-ref:' . env('BP_DEV_REF');
                    $headers[] = 'brightpearl-app-ref:' . env('BP_CLIENT_ID');

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else if (strtolower($methodType) == 'get') {
                    /* CURL GET METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'Authorization: Bearer ' . $access_token;
                    $headers[] = 'brightpearl-dev-ref:' . env('BP_DEV_REF');
                    $headers[] = 'brightpearl-app-ref:' . env('BP_CLIENT_ID');

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                } else {
                    /* CURL GET METHOD */
                    curl_setopt($ch, CURLOPT_URL, $baseUrl);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $methodType);

                    $headers = array();
                    $headers[] = "Content-Type: application/json";
                    $headers[] = 'Authorization: Bearer ' . $access_token;
                    $headers[] = 'brightpearl-dev-ref:' . env('BP_DEV_REF');
                    $headers[] = 'brightpearl-app-ref:' . env('BP_CLIENT_ID');

                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

                    if (!$result = curl_exec($ch)) {
                        $result = curl_error($ch);
                    }
                    curl_close($ch);
                    return $result;
                }
            }
        }

        return $result;
    }

    /* Get Brightpeal Credentials */
    public function getBPCredentials($accountCode){
        $exist = DB::table('api_config')->select('id', 'organization_id', 'account_name', 'account_token', 'status', 'sync_completed', 'env_type')
        ->where('status', '1')
        ->where(function ($query1) use ($accountCode) {
            $query1->where('account_name', $accountCode)->Orwhere('organization_id', $accountCode);
        })->first();

        if ($exist) {
            return $exist;
        }
        return false;
    }

    /* Multi Message Url Call Method */
    public function multiMessage($OrgId = NULL, $PostData = NULL){
        $result = false;
        if ($OrgId) {
            $url = "/multi-message";

            $result = $this->callCurlMethod('POST', $url, $OrgId, $PostData);
            if ($this->is_json($result, $return_data = false)) {
                $decode_val = json_decode($result, true);
                return $result = $decode_val;
                //!isset($decode_val['errors']) ? $decode_val['response'] : $decode_val['errors'];
            } else {
                return false;
            }
        }
        return $result;
    }

    /* Create Brightpearl webhooks with Multi Message Url Call Method */
    public function createWebHooks($user_id){
        $url = '/integration-service/webhook/';
        $Baseurl = env('APP_WEBHOOK_URL');

        $webhook_order_status_modified=[
            "label"=>"order_status_modified",
            "uri"=> $url,
            "httpMethod"=> "POST",
            "body"=> [
                'subscribeTo' => 'order.modified.order-status',
                'httpMethod' => 'POST',
                'uriTemplate' => $Baseurl . "/webhook.php?id=" . $user_id . "&event=order_status_modified",
                'bodyTemplate' => '{ "accountCode": "${account-code}", "resourceType": "${resource-type}", "id": "${resource-id}", "lifecycleEvent": "${lifecycle-event}", "fullEvent": "${full-event}", "raisedOn": "${raised-on}", "brightpearlVersion": "${brightpearl-version}" }',
                'contentType' => 'application/json',
                'idSetAccepted' => true,
                'qualityOfService' => 1
            ]
        ];

        $webhook_goods_out_note_created =[
            "label"=>"goods_out_note_created",
            "uri"=> $url,
            "httpMethod"=> "POST",
            "body"=> [
                'subscribeTo' => 'goods-out-note.created',
                'httpMethod' => 'POST',
                'uriTemplate' => $Baseurl . "/webhook.php?id=" . $user_id . "&event=goods_out_note_created",
                'bodyTemplate' => '{ "accountCode": "${account-code}", "resourceType": "${resource-type}", "id": "${resource-id}", "lifecycleEvent": "${lifecycle-event}", "fullEvent": "${full-event}", "raisedOn": "${raised-on}", "brightpearlVersion": "${brightpearl-version}" }',
                'contentType' => 'application/json',
                'idSetAccepted' => true,
                'qualityOfService' => 1
            ]
        ];

        $webhook_goods_out_note_destroyed =[
            "label"=>"goods_out_note_destroyed",
            "uri"=> $url,
            "httpMethod"=> "POST",
            "body"=> [
                'subscribeTo' => 'goods-out-note.destroyed',
                'httpMethod' => 'POST',
                'uriTemplate' => $Baseurl . "/webhook.php?id=" . $user_id . "&event=goods_out_note_destroyed",
                'bodyTemplate' => '{ "accountCode": "${account-code}", "resourceType": "${resource-type}", "id": "${resource-id}", "lifecycleEvent": "${lifecycle-event}", "fullEvent": "${full-event}", "raisedOn": "${raised-on}", "brightpearlVersion": "${brightpearl-version}" }',
                'contentType' => 'application/json',
                'idSetAccepted' => true,
                'qualityOfService' => 1
            ]
        ];

        $MainMessage=[
            $webhook_order_status_modified,
            $webhook_goods_out_note_created,
            $webhook_goods_out_note_destroyed
        ];

        $post=[
            "processingMode"=> "SEQUENTIAL",
            "onFail"=> "CONTINUE",
            "messages"=>$MainMessage
        ];

        $order=$this->multiMessage($user_id,json_encode($post));
        $success=true;

        if($order && isset($order['response']['processedMessages'])){
            foreach($order['response']['processedMessages'] as $key=>$res){
                $contentbody = json_decode($res['body']['content'],true);

                /* Error Handling 1 */
                if (isset($contentbody['error'])  ||  (isset($contentbody['errors'])  && !isset($contentbody['errors'][0]['code']))) {
                        if(isset($contentbody['errors'][0]['message'])){
                            $message = $contentbody['errors'][0]['message'];
                        }else{
                            $message = $contentbody['error'];
                        }
                        $success=false;
                }

                /* Error Handling 2 */
                if (isset($contentbody['errors']) &&   isset($contentbody['errors'][0]['message'])) {
                    if(isset($contentbody['errors'][0]['message'])){
                        $message = $contentbody['errors'][0]['message'];
                    }else{
                        $message = $contentbody['errors'];
                    }
                    $success=false;
                }

                /* Response Handler */
                if(isset($contentbody['response'])){
                    $this->obj->makeInsertGetId('brightpearl_webhooks',['organization_id'=>$user_id,'webhook_id'=>$contentbody['response']]);
                }
            }
        }
        return $success;
    }

    public function deleteWebHooks($user_id){
        $url = '/integration-service/webhook/';

        $res = DB::table('brightpearl_webhooks')->where(['organization_id'=>$user_id])->get();

        $multi_arr = [];
        if(count($res)){
            foreach($res as $k => $v){
                $web_arr = [
                    "label"=>$v->id,
                    "uri"=> $url.'/'.$v->webhook_id,
                    "httpMethod"=> "DELETE",
                    "body"=> []
                ];
                array_push($multi_arr,$web_arr);
            }

            $MainMessage=$multi_arr;

            $post=[
                "processingMode"=> "SEQUENTIAL",
                "onFail"=> "CONTINUE",
                "messages"=>$MainMessage
            ];

            $order = $this->multiMessage($user_id, json_encode($post));
            $success = true;

            if($order && isset($order['response']['processedMessages'])){
                foreach($order['response']['processedMessages'] as $key=>$res){
                $contentbody = json_decode($res['body']['content'],true);

                /* Error Handling 1 */
                if (isset($contentbody['error'])  ||  (isset($contentbody['errors'])  && !isset($contentbody['errors'][0]['code']))) {
                    if(isset($contentbody['errors'][0]['message'])){
                        $message = $contentbody['errors'][0]['message'];
                    }else{
                        $message = $contentbody['error'];
                    }
                    $success=false;
                }

                /* Error Handling 2 */
                if (isset($contentbody['errors']) &&   isset($contentbody['errors'][0]['message'])) {
                    if(isset($contentbody['errors'][0]['message'])){
                        $message = $contentbody['errors'][0]['message'];
                    }else{
                        $message = $contentbody['errors'];
                    }
                    $success=false;
                }

                /* Response Handler */
                    if(!isset($contentbody['response']) && isset($res['label'])){
                        $this->obj->makeDelete('brightpearl_webhooks',['id'=>$res['label']]);
                    }
                }
            }
        }else{
            $success=false;
        }
        return $success;
    }

    public function DateTimeDiff($start, $end){
        $startTime = Carbon::parse($start);
        $finishTime = Carbon::parse($end);
        $totalDuration = $finishTime->diff($startTime)->days; //->format('%H:%I:%S');

        if ($totalDuration <= 2) {
            return true;
        } else {
            return false;
        }
    }

    public function refreshBrightpearlToken(){
        $acc = DB::table('api_config')->whereNotNull('refresh_token')->whereNotNull('account_name')->get();
        foreach ($acc as $accounts) {
            $org_id = $accounts->organization_id;
            date_default_timezone_set("America/Phoenix");
            if (isset($accounts->expires_in) && isset($accounts->token_refresh_time)) {
                $dateAdd = Carbon::parse(date('Y-m-d H:i:s', $accounts->token_refresh_time));
                $start = $dateAdd->addSeconds($accounts->expires_in);
                $end = Carbon::now();
                if ($this->DateTimeDiff($start, $end)) {
                    $client_id = env('BP_CLIENT_ID');
                    $client_secret = env('BP_CLIENT_SECRET');
                    $refresh_token = base64_decode($accounts->refresh_token);
                    $service_url = 'https://oauth.brightpearl.com/token/' . $accounts->account_name;
                    $curl = curl_init($service_url);
                    $curl_post_data = array(
                        'client_id' => $client_id,
                        'client_secret' => $client_secret,
                        'refresh_token' => $refresh_token,
                        'grant_type' => 'refresh_token',
                    );
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                    curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($curl_post_data));
                    $response = curl_exec($curl);
                    curl_close($curl);
                    $decode_val = json_decode($response, true);

                    if (isset($decode_val['access_token'])) {
                        $OauthData = [
                            'account_token' => base64_encode($decode_val['access_token']),
                            'refresh_token' => base64_encode($decode_val['refresh_token']),
                            'api_domain' => $decode_val['api_domain'],
                            'token_type' => $decode_val['token_type'],
                            'expires_in' => $decode_val['expires_in'],
                            'token_refresh_time' => time()
                        ];
                        DB::table('api_config')->where('organization_id', $org_id)->update($OauthData);
                    }
                }
            }
        }
    }

    public static function checkEmoji($str){
        $regexEmoticons = '/[\x{1F600}-\x{1F64F}]/u';
        preg_match($regexEmoticons, $str, $matches_emo);
        if (!empty($matches_emo[0])) {
            // print_r($matches_emo[0]);
            return true;
        }

        // Match Miscellaneous Symbols and Pictographs
        $regexSymbols = '/[\x{1F300}-\x{1F5FF}]/u';
        preg_match($regexSymbols, $str, $matches_sym);
        if (!empty($matches_sym[0])) {
            // print_r($matches_sym[0]);
            return true;
        }

        // Match Transport And Map Symbols
        $regexTransport = '/[\x{1F680}-\x{1F6FF}]/u';
        preg_match($regexTransport, $str, $matches_trans);
        if (!empty($matches_trans[0])) {
            //print_r($matches_trans[0]);
            return true;
        }

        // Match Miscellaneous Symbols
        $regexMisc = '/[\x{2600}-\x{26FF}]/u';
        preg_match($regexMisc, $str, $matches_misc);
        if (!empty($matches_misc[0])) {
            // print_r($matches_misc[0]);
            return true;
        }

        // Match Dingbats
        $regexDingbats = '/[\x{2700}-\x{27BF}]/u';
        preg_match($regexDingbats, $str, $matches_bats);
        if (!empty($matches_bats[0])) {
            // print_r($matches_bats[0]);
            return true;
        }
        // Match  you can rip out everything from 0-31 and 127-255 with this:
        $regexUnwant = '/[\x00-\x1F\x7F-\xFF]/';
        preg_match($regexUnwant, $str, $matches_unwant);
        if (!empty($matches_unwant[0])) {
            // print_r($matches_unwant[0]);
            return true;
        }

        return false;
    }

    /* Check Valid Json */
    public function is_json($string, $return_data = false){
        $data = json_decode($string);
        return (json_last_error() == JSON_ERROR_NONE) ? ($return_data ? $data : true) : false;
    }

    public function initialSyncExistingProductsBP($accountCode,$allow_updatedon_date=1) {
        $initial_synced = 'No';

        $limit = 10;
        $getUris = $this->obj->getResultByConditions('brightpearl_urls', ['status' => 0, 'organization_id' => $accountCode, 'url_name' => 'product'], ['id', 'url'], ['created_at' => 'asc'], $limit);

        foreach($getUris as $getUri){
            $url = '/product-service'.$getUri->url.'?includeOptional=customFields,nullCustomFields';
            $result2 = $this->callCurlMethod('GET',$url, $accountCode);
            $data1 = json_decode($result2, 1);
            if (isset($data1['response'])) {
                $Items = $data1['response'];
                if(count($Items)){
                    foreach ($Items as $detail) {

                        if (isset($detail['salesChannels'][0]['productName']) && trim($detail['salesChannels'][0]['productName'])) {
                            $id = $detail['id'];

                            if (!self::checkEmoji($detail['identity']['sku'])) {

                                $fields = array(
                                    'organization_id' => $accountCode,
                                    'item_id' => $id,
                                    'sku' => !empty($detail['identity']['sku']) ? $detail['identity']['sku'] : NULL,
                                    'name' => !empty($detail['salesChannels'][0]['productName']) ? $detail['salesChannels'][0]['productName'] : NULL,
                                    'isbn' => isset($detail['identity']['isbn']) ? $detail['identity']['isbn'] : NULL,
                                    'ean' => isset($detail['identity']['ean']) ? $detail['identity']['ean'] : NULL,
                                    'upc' => isset($detail['identity']['upc']) ? $detail['identity']['upc'] : NULL,
                                    'mpn' => isset($detail['identity']['mpn']) ? $detail['identity']['mpn'] : NULL,
                                    'barcode' => isset($detail['identity']['barcode']) ? $detail['identity']['barcode'] : NULL,
                                    'status' => $detail['status'],
                                    'createdOn' => isset($detail['createdOn']) ? $detail['createdOn'] : NULL,
                                    'updatedOn' => isset($detail['updatedOn']) ? $detail['updatedOn'] : NULL
                                );

                            }

                            $existing = $this->obj->getFirstResultByConditions('brightpearl_product', ['item_id'=>$id, 'organization_id'=>$accountCode], ['id']);

                            if ( $existing ) {

                                if (!empty($fields)) {
                                    $fields['is_deleted'] = 0;
                                    $fields['product_sync_status'] = 'pending';
                                    $this->obj->makeUpdate('brightpearl_product', ['id'=>$existing->id], $fields);
                                }

                            } else {

                                if (!empty($fields)) {
                                    $this->obj->makeInsert('brightpearl_product', $fields);
                                }

                            }

                            $fields = []; // clear array after use

                        }

                    }

                    $this->obj->makeUpdate('brightpearl_urls', ['status' => 1], ['id' => $getUri->id]);
                }
            } else {
                if ( isset($data1['errors'][0]['code']) && $data1['errors'][0]['code'] == 'CMNC-404') {
                    $initial_synced = 'Yes';
                }
            }
        }

        return $initial_synced;
    }

    /* Get Goods Out Notes In BP */
    public function getGoodsOutNotes($org_id, $url = NULL, $GoodsOutNoteID = NULL){
        $response = false;
        if( !isset($url) ){
            $url = "/warehouse-service/order/*/goods-note/goods-out/{$GoodsOutNoteID}";
        }

        $response = $this->callCurlMethod('GET', $url, $org_id);
        return $response;
    }

    /* Get Order In BP */
    public function getOrder($org_id, $OrderID = NULL, $type = "normal", $url = NULL){
        if ($type == "search") {
            $url =  "/order-service/{$url}";
        } elseif ($type == "normal" || $type == "") {
            $url =  "/order-service/order/$OrderID?includeOptional=customFields";
        }

        $response = $this->callCurlMethod('GET', $url, $org_id);
        return $response;
    }

    /* get warehouse for daily */
    public function getWarehouse($OrgId){
        $url = '/warehouse-service/warehouse';

        $result = $this->callCurlMethod('GET', $url, $OrgId);
        if ($this->is_json($result, $return_data = false)) {
            $decode_val = json_decode($result, true);
            if (isset($decode_val['response']) || !empty($decode_val['response'])) {
                return $decode_val['response'];
            }
            return 0;
        }

        return 0;
    }

    /* brithpearl update warehouse  cron */
    public function storeWarehouse($OrgId){
        $WH = self::getWarehouse($OrgId);

        if ($WH) {
            $removingwarehouseId = [];
            foreach($WH as $key) {

                array_push($removingwarehouseId, $key['id']);
                $exist = DB::table('brightpearl_warehouses')->where('warehouseId', $key['id'])->where('organization_id', $OrgId)->first();
                if ($exist) {
                    DB::table('brightpearl_warehouses')->where('warehouseId', $key['id'])->where('organization_id', $OrgId)->update([
                        'name' => $key['name']
                    ]);
                } else {
                    DB::table('brightpearl_warehouses')->insert([
                        'warehouseId' => $key['id'],
                        'organization_id' => $OrgId,
                        'name' => $key['name']
                    ]);
                }
            }
            DB::table('brightpearl_warehouses')->whereNotIn('warehouseId', $removingwarehouseId)->where('organization_id', $OrgId)->delete();
        }
    }

    public function getOrderstatus($OrgId)
    {
        $url = '/order-service/order-status';

        $result = $this->callCurlMethod('GET', $url, $OrgId);
        if ($this->is_json($result, $return_data = false)) {
            $decode_val = json_decode($result, true);
            if (isset($decode_val['response']) || !empty($decode_val['response'])) {
                return $decode_val['response'];
            }
            return 0;
        }

        return 0;
    }

    /* brithpearl order status update cron */
    public function storeOrderStatus($OrgId)
    {
        $OS = self::getOrderstatus($OrgId);
        if ($OS) {
            $removingOSId = [];
            foreach ($OS as $key) {
                array_push($removingOSId, $key['statusId']);
                $exist = DB::table('brightpearl_order_status')->where('statusId', $key['statusId'])->where('organization_id', $OrgId)->first();
                if ($exist) {
                    DB::table('brightpearl_order_status')->where('statusId', $key['statusId'])->where('organization_id', $OrgId)->update([

                        'name' => $key['name'],
                        'orderTypeCode' => $key['orderTypeCode'],
                        'disabled' => $key['disabled'],
                        'visible' => $key['visible'],
                    ]);
                } else {
                    DB::table('brightpearl_order_status')->insert([
                        'statusId' => $key['statusId'],
                        'organization_id' => $OrgId,
                        'name' => $key['name'],
                        'orderTypeCode' => $key['orderTypeCode'],
                        'disabled' => $key['disabled'],
                        'visible' => $key['visible'],
                    ]);
                }
            }
            DB::table('brightpearl_order_status')->whereNotIn('statusId', $removingOSId)->where('organization_id', $OrgId)->delete();
        }
    }

    public function getShippingMethods($OrgId)
    {
        $url = '/warehouse-service/shipping-method';

        $result = $this->callCurlMethod('GET', $url, $OrgId);
        if ($this->is_json($result, $return_data = false)) {
            $decode_val = json_decode($result, true);
            if (isset($decode_val['response']) || !empty($decode_val['response'])) {
                return $decode_val['response'];
            }
            return 0;
        }

        return 0;
    }

    public function getPriceList($OrgId)
    {
        $url = '/product-service/price-list/';

        $result = $this->callCurlMethod('GET', $url, $OrgId);
        if ($this->is_json($result, $return_data = false)) {
            $decode_val = json_decode($result, true);
            return !isset($decode_val['errors']) ? $decode_val['response'] : false;
        } else {
            return false;
        }
    }

    /* brithpearl order status update cron */
    public function storeShippingMethods($OrgId)
    {
        $OS = self::getShippingMethods($OrgId);
        if ($OS) {
            $removingOSId = [];
            foreach ($OS as $key) {
                array_push($removingOSId, $key['id']);
                $exist = DB::table('brightpearl_shipping_methods')->where('shipment_method_id', $key['id'])->where('organization_id', $OrgId)->first();
                if ($exist) {
                    DB::table('brightpearl_shipping_methods')->where('shipment_method_id', $key['id'])->where('organization_id', $OrgId)->update([
                        'name' => $key['name'],
                        'methodType' => $key['methodType']
                    ]);
                } else {
                    DB::table('brightpearl_shipping_methods')->insert([
                        'organization_id' => $OrgId,
                        'shipment_method_id' => $key['id'],
                        'name' => $key['name'],
                        'methodType' => $key['methodType']
                    ]);
                }
            }
            DB::table('brightpearl_shipping_methods')->whereNotIn('shipment_method_id', $removingOSId)->where('organization_id', $OrgId)->delete();
        }
    }

    public function storePriceList($OrgId)
    {
        $OS = self::getPriceList($OrgId);
        if ($OS) {
            $removingOSId = [];
            foreach ($OS as $key) {
                array_push($removingOSId, $key['id']);
                $exist = DB::table('brightpearl_price_list')->where('price_list_id', $key['id'])->where('organization_id', $OrgId)->first();
                if ($exist) {
                    DB::table('brightpearl_price_list')->where('price_list_id', $key['id'])->where('organization_id', $OrgId)->update([
                        'name' => isset($key['name']) && isset($key['name']['text'])?$key['name']['text']:'',
                        'code' => isset($key['code'])?$key['code']:'',
                    ]);
                } else {
                    DB::table('brightpearl_price_list')->insert([
                        'organization_id' => $OrgId,
                        'price_list_id' => $key['id'],
                        'name' => isset($key['name']) && isset($key['name']['text'])?$key['name']['text']:'',
                        'code' => isset($key['code']) ?$key['code']:'',
                    ]);
                }
            }
            DB::table('brightpearl_price_list')->whereNotIn('price_list_id', $removingOSId)->where('organization_id', $OrgId)->delete();
        }
    }

    public function handleResponseError($response)
    {
        $errors_list = null;
        if (isset($response['errors'])) {
            foreach ($response['errors'] as $key => $error) {
                $errors_list .= $error['message'] . ",";
            }
        } else if (isset($response['response'])) {

            if ( is_string($response['response']) ) {
                $errors_list = $response['response'];
            }
        }

        return rtrim($errors_list, ",");
    }

    /* Custom Errors */
    public function MakeCustomError($Error,$GON)
    {
        $pattern=["You have provided an invalid goods-out note Id:","it has been shipped."];//Error pattern which is comes form BP
        $return = ["status" => 0, "status_text" => $Error];
        foreach ($pattern as $err) {
            if (strpos($Error, $err) !== false) {
                $Error=preg_replace('/[0-9]+/', $GON, $Error);//replace number with GON
                $return = ["status" => 1, "status_text" => $Error];
                break;
            }
        }

        return $return;
    }

    /* Put Goods Out Notes In BP */
    public function putGoodsOutNotes($OrgId, $url = NULL, array $postData, $GoodsOutNoteID = NULL)
    {
        $url = "/warehouse-service/goods-note/goods-out/{$GoodsOutNoteID}";

        $result = $this->callCurlMethod('PUT', $url, $OrgId, json_encode($postData, true));
        if ($result) {
            return $result;
        }
        return 0;
    }

    /* Post Goods Out Note Event In BP */
    public function postGoodsOutNoteEvent($OrgId, $url = NULL, array $postData, $GoodsOutNoteID = NULL)
    {
        $url = "/warehouse-service/goods-note/goods-out/{$GoodsOutNoteID}/event";

        $result = $this->callCurlMethod('POST', $url, $OrgId, json_encode($postData, true));
        if ($result) {
            return $result;
        }
        return 0;
    }

    public function GetWebhookList($OrgId)
    {
        $url = "/integration-service/webhook";

        $result = $this->callCurlMethod('GET', $url, $OrgId, []);
        if ($result) {
            dd( json_decode($result, true) );
        }
    }

    public function GetProductDetails($OrgId, $ProductIdSet = NULL)
    {
        if( isset($ProductIdSet) ){
            $url = "/product-service/product/{$ProductIdSet}?includeOptional=customFields,nullCustomFields";

            $result = $this->callCurlMethod('GET', $url, $OrgId, []);
            if ($product = json_decode($result, true)) {
                return $product;
            }
        }
        return 0;
    }

    public function GetProductPrice($OrgId, $ProductIdSet = NULL, $PriceListId = NULL)
    {
        if( isset($ProductIdSet) && isset($PriceListId) ){
            $url = "/product-service/product-price/{$ProductIdSet}/price-list/{$PriceListId}";

            $result = $this->callCurlMethod('GET', $url, $OrgId, []);
            if ($product = json_decode($result, true)) {
                return $product;
            }
        }
        return 0;
    }

}
