<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use DB;
use Illuminate\Support\Facades\Cache;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\CmdBrightpearlRefreshToken::class,
        \App\Console\Commands\CmdCheckBpInitialSync::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        \Storage::put('cron_log.txt', 'Cron called at: ' . now());
        $schedule->command('Cron:CmdBrightpearlRefreshToken')->cron('*/8 * * * *');
        $schedule->command('Cron:CmdCheckBpInitialSync')->cron('*/2 * * * *');

		$instance_count = 1;
		/*$users = Cache::remember('cron_user_data', (40*60), function () {
            return DB::table('users')->select('id')->where(['status'=>1,'role'=>'admin'])->get();
        });*/
        $users = DB::table('users')->select('id')->where(['status'=>1, 'role'=>'admin'])->get();

        \Storage::append('cron_log.txt', 'Cron users: ' . print_r($users, true));
		foreach($users as $uk => $uv){

            //$bp_info = Cache::remember('cron_brightpearl_info_'.$uv->id, (30*60), function () use($uv) {
                $bp_info = DB::table('api_config')->select('api_config.organization_id')
                ->where(['api_config.organization_id'=>$uv->id, 'api_config.status'=>1, 'api_config.sync_completed'=>1])
                ->first();
            //});
            \Storage::append('cron_log.txt', 'Cron bp_info: ' . print_r($bp_info, true));

            for($ci=1; $ci<=$instance_count; $ci++){
                if( $bp_info ){ // If Shopify & Brightpearl ac is Active & Initial sync completed
                    \Storage::append('cron_log.txt', 'Cron calling functions: ' . print_r($uv->id, true));
                    $schedule->call('App\Http\Controllers\BrightpearlController@getSOGoodOutNoteCreated', [$uv->id])->name('getSOGoodOutNoteCreated' . $uv->id)->withoutOverlapping()->cron('*/5 * * * *');
                    $schedule->call('App\Http\Controllers\BrightpearlController@syncTrackingInformation', [$uv->id, 0])->name('syncTrackingInformation' . $uv->id)->withoutOverlapping()->cron('*/7 * * * *');
                    $schedule->call('App\Http\Controllers\BrightpearlController@getGoodOutNoteBackup', [$uv->id])->name('getGoodOutNoteBackup' . $uv->id)->withoutOverlapping()->cron('0 */1 * * *'); // Every one hour
                }
            }
        }

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
