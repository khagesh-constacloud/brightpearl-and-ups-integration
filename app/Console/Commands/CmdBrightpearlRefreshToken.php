<?php
    namespace App\Console\Commands;
    use Illuminate\Console\Command;

    class CmdBrightpearlRefreshToken extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */

        protected $signature = 'Cron:CmdBrightpearlRefreshToken';

        /**
         * The console command description.
         *
         * @var string
         */

        protected $description = '';

        /**
         * Create a new command instance.
         *
         * @return void
         */

        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */

        public function handle()
        {
            app('App\Helper\BrightpearlApi')->refreshBrightpearlToken();
        }
    }